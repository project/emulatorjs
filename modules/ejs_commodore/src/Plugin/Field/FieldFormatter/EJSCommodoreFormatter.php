<?php

namespace Drupal\ejs_commodore\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Commodore core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_commodore",
 *   label = @Translation("EmulatorJS Commodore"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSCommodoreFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'vice_x64sc';

    $settings['vice_options'] = [
      'vice_c128_model' => "C128 PAL",
      'vice_drive_sound_emulation' => "20%",
      'vice_manual_crop_bottom' => "0",
      'vice_manual_crop_left' => "0",
      'vice_manual_crop_right' => "0",
      'vice_manual_crop_top' => "0",
      'vice_resid_8580filterbias' => "1500",
      'vice_resid_filterbias' => "500",
      'vice_resid_gain' => "97",
      'vice_resid_passband' => "90",
      'vice_sid_engine' => "ReSID",
      'vice_sound_sample_rate' => "48000",
      'vice_vkbd_dimming' => "25%",
      'vice_vkbd_transparency' => "25%",
      'vice_zoom_mode_crop' => "deprecated",
    ];

    $settings['puae_options'] = [
      'puae_sound_stereo_separation' => "100%",
      'puae_sound_volume_cd' => "100%",
      'puae_vkbd_dimming' => "25%",
      'puae_vkbd_transparency' => "25%",
      'puae_zoom_mode_crop' => "deprecated",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'Commodore 64' => [
        'vice_x64sc' => "vice_x64sc",
        'vice_x64' => "vice_x64",
      ],
      'Commodore 128' => [
        'vice_x128' => "vice_x128",
      ],
      'Commodore Amiga' => [
        'puae' => "puae",
      ],
      'Commodore PET' => [
        'vice_xpet' => "vice_xpet",
      ],
      'Commodore Plus/4' => [
        'vice_xplus4' => "vice_xplus4",
      ],
      'Commodore VIC-20' => [
        'vice_xvic' => "vice_xvic",
      ],
    ];

    // Vice_x64sc options.
    $vice_options = $this->getSetting('vice_options');
    $form['vice_options'] = [
      '#type' => 'details',
      '#title' => 'vice Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
            ['value' => 'vice_xpet'],
            ['value' => 'vice_xplus4'],
            ['value' => 'vice_xvic'],
          ],
        ],
      ],
    ];

    $form['vice_options']['vice_c128_model'] = [
      '#type' => 'select',
      '#title' => 'vice_c128_model',
      '#options' => [
        'C128 D NTSC' => "C128 D NTSC",
        'C128 D PAL' => "C128 D PAL",
        'C128 DCR NTSC' => "C128 DCR NTSC",
        'C128 DCR PAL' => "C128 DCR PAL",
        'C128 NTSC' => "C128 NTSC",
        'C128 PAL' => "C128 PAL",
      ],
      '#default_value' => $vice_options['vice_c128_model'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'vice_x128'],
        ],
      ],
    ];

    $form['vice_options']['vice_drive_sound_emulation'] = [
      '#type' => 'select',
      '#title' => 'vice_drive_sound_emulation',
      '#options' => [],
      '#default_value' => $vice_options['vice_drive_sound_emulation'],
    ];
    for ($i = 5; $i <= 100; $i += 5) {
      $form['vice_options']['vice_drive_sound_emulation']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['vice_options']['vice_manual_crop_bottom'] = [
      '#type' => 'select',
      '#title' => 'vice_manual_crop_bottom',
      '#options' => [],
      '#default_value' => $vice_options['vice_manual_crop_bottom'],
    ];
    for ($i = 0; $i <= 60; $i++) {
      $form['vice_options']['vice_manual_crop_bottom']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_manual_crop_left'] = [
      '#type' => 'select',
      '#title' => 'vice_manual_crop_left',
      '#options' => [],
      '#default_value' => $vice_options['vice_manual_crop_left'],
    ];
    for ($i = 0; $i <= 60; $i++) {
      $form['vice_options']['vice_manual_crop_left']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_manual_crop_right'] = [
      '#type' => 'select',
      '#title' => 'vice_manual_crop_right',
      '#options' => [],
      '#default_value' => $vice_options['vice_manual_crop_right'],
    ];
    for ($i = 0; $i <= 60; $i++) {
      $form['vice_options']['vice_manual_crop_right']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_manual_crop_top'] = [
      '#type' => 'select',
      '#title' => 'vice_manual_crop_top',
      '#options' => [],
      '#default_value' => $vice_options['vice_manual_crop_top'],
    ];
    for ($i = 0; $i <= 60; $i++) {
      $form['vice_options']['vice_manual_crop_top']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_resid_8580filterbias'] = [
      '#type' => 'select',
      '#title' => 'vice_resid_8580filterbias',
      '#options' => [],
      '#default_value' => $vice_options['vice_resid_8580filterbias'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
          ],
        ],
      ],
    ];
    for ($i = 0; $i <= 5000; $i += 500) {
      $form['vice_options']['vice_resid_8580filterbias']['#options'][strval($i)] = strval($i);
    }
    for ($i = -500; $i >= -5000; $i -= 500) {
      $form['vice_options']['vice_resid_8580filterbias']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_resid_filterbias'] = [
      '#type' => 'select',
      '#title' => 'vice_resid_filterbias',
      '#options' => [],
      '#default_value' => $vice_options['vice_resid_filterbias'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
          ],
        ],
      ],
    ];
    for ($i = 0; $i <= 5000; $i += 500) {
      $form['vice_options']['vice_resid_filterbias']['#options'][strval($i)] = strval($i);
    }
    for ($i = -500; $i >= -5000; $i -= 500) {
      $form['vice_options']['vice_resid_filterbias']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_resid_gain'] = [
      '#type' => 'select',
      '#title' => 'vice_resid_gain',
      '#options' => [],
      '#default_value' => $vice_options['vice_resid_gain'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
          ],
        ],
      ],
    ];
    for ($i = 90; $i <= 100; $i++) {
      $form['vice_options']['vice_resid_gain']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_resid_passband'] = [
      '#type' => 'select',
      '#title' => 'vice_resid_passband',
      '#options' => [],
      '#default_value' => $vice_options['vice_resid_passband'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
          ],
        ],
      ],
    ];
    for ($i = 0; $i <= 100; $i += 10) {
      $form['vice_options']['vice_resid_passband']['#options'][strval($i)] = strval($i);
    }

    $form['vice_options']['vice_sid_engine'] = [
      '#type' => 'select',
      '#title' => 'vice_sid_engine',
      '#options' => [
        'FastSID' => "FastSID",
        'ReSID' => "ReSID",
        'ReSID-FP' => "ReSID-FP",
      ],
      '#default_value' => $vice_options['vice_sid_engine'],
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => [
            ['value' => 'vice_x64sc'],
            ['value' => 'vice_x64'],
            ['value' => 'vice_x128'],
          ],
        ],
      ],
    ];

    $form['vice_options']['vice_sound_sample_rate'] = [
      '#type' => 'select',
      '#title' => 'vice_sound_sample_rate',
      '#options' => [
        '22050' => "22050",
        '44100' => "44100",
        '48000' => "48000",
        '96000' => "96000",
      ],
      '#default_value' => $vice_options['vice_sound_sample_rate'],
    ];

    $form['vice_options']['vice_vkbd_dimming'] = [
      '#type' => 'select',
      '#title' => 'vice_vkbd_dimming',
      '#options' => [],
      '#default_value' => $vice_options['vice_vkbd_dimming'],
    ];
    for ($i = 0; $i <= 100; $i += 25) {
      $form['vice_options']['vice_vkbd_dimming']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['vice_options']['vice_vkbd_transparency'] = [
      '#type' => 'select',
      '#title' => 'vice_vkbd_transparency',
      '#options' => [],
      '#default_value' => $vice_options['vice_vkbd_transparency'],
    ];
    for ($i = 0; $i <= 100; $i += 25) {
      $form['vice_options']['vice_vkbd_transparency']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['vice_options']['vice_zoom_mode_crop'] = [
      '#type' => 'select',
      '#title' => 'vice_zoom_mode_crop',
      '#options' => [
        '4:3' => "4:3",
        '5:4' => "5:4",
        '16:9' => "16:9",
        '16:10' => "16:10",
        'deprecated' => "deprecated",
      ],
      '#default_value' => $vice_options['vice_zoom_mode_crop'],
    ];

    // Puae options.
    $puae_options = $this->getSetting('puae_options');
    $form['puae_options'] = [
      '#type' => 'details',
      '#title' => 'puae Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'puae'],
        ],
      ],
    ];

    $form['puae_options']['puae_sound_stereo_separation'] = [
      '#type' => 'select',
      '#title' => 'puae_sound_stereo_separation',
      '#options' => [],
      '#default_value' => $puae_options['puae_sound_stereo_separation'],
    ];
    for ($i = 0; $i <= 100; $i += 10) {
      $form['puae_options']['puae_sound_stereo_separation']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['puae_options']['puae_sound_volume_cd'] = [
      '#type' => 'select',
      '#title' => 'puae_sound_volume_cd',
      '#options' => [],
      '#default_value' => $puae_options['puae_sound_volume_cd'],
    ];
    for ($i = 0; $i <= 100; $i += 5) {
      $form['puae_options']['puae_sound_volume_cd']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['puae_options']['puae_vkbd_dimming'] = [
      '#type' => 'select',
      '#title' => 'puae_vkbd_dimming',
      '#options' => [],
      '#default_value' => $puae_options['puae_vkbd_dimming'],
    ];
    for ($i = 0; $i <= 100; $i += 25) {
      $form['puae_options']['puae_vkbd_dimming']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['puae_options']['puae_vkbd_transparency'] = [
      '#type' => 'select',
      '#title' => 'puae_vkbd_transparency',
      '#options' => [],
      '#default_value' => $puae_options['puae_vkbd_transparency'],
    ];
    for ($i = 0; $i <= 100; $i += 25) {
      $form['puae_options']['puae_vkbd_transparency']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['puae_options']['puae_zoom_mode_crop'] = [
      '#type' => 'select',
      '#title' => 'puae_zoom_mode_crop',
      '#options' => [
        '4:3' => "4:3",
        '5:4' => "5:4",
        '16:9' => "16:9",
        '16:10' => "16:10",
        'deprecated' => "deprecated",
      ],
      '#default_value' => $puae_options['puae_zoom_mode_crop'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-commodore-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-commodore ejs-commodore-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_commodore/commodore'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'vice_xpet':
        case 'vice_xplus4':
        case 'vice_xvic':
          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_drive_sound_emulation'] = $vice_options['vice_drive_sound_emulation'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_bottom'] = $vice_options['vice_manual_crop_bottom'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_left'] = $vice_options['vice_manual_crop_left'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_right'] = $vice_options['vice_manual_crop_right'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_top'] = $vice_options['vice_manual_crop_top'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_sound_sample_rate'] = $vice_options['vice_sound_sample_rate'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_vkbd_dimming'] = $vice_options['vice_vkbd_dimming'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_vkbd_transparency'] = $vice_options['vice_vkbd_transparency'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_zoom_mode_crop'] = $vice_options['vice_zoom_mode_crop'];

          break;

        case 'vice_x64sc':
        case 'vice_x64':
          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_drive_sound_emulation'] = $vice_options['vice_drive_sound_emulation'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_bottom'] = $vice_options['vice_manual_crop_bottom'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_left'] = $vice_options['vice_manual_crop_left'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_right'] = $vice_options['vice_manual_crop_right'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_manual_crop_top'] = $vice_options['vice_manual_crop_top'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_resid_8580filterbias'] = $vice_options['vice_resid_8580filterbias'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_resid_filterbias'] = $vice_options['vice_resid_filterbias'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_resid_gain'] = $vice_options['vice_resid_gain'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_resid_passband'] = $vice_options['vice_resid_passband'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_sid_engine'] = $vice_options['vice_sid_engine'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_sound_sample_rate'] = $vice_options['vice_sound_sample_rate'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_vkbd_dimming'] = $vice_options['vice_vkbd_dimming'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_vkbd_transparency'] = $vice_options['vice_vkbd_transparency'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['vice_zoom_mode_crop'] = $vice_options['vice_zoom_mode_crop'];

          break;

        case 'vice_x128':
          $vice_options = $this->getSetting('vice_options');

          foreach ($vice_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'puae':
          $puae_options = $this->getSetting('puae_options');

          foreach ($puae_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
