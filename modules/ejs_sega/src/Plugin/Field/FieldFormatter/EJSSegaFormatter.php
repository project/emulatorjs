<?php

namespace Drupal\ejs_sega\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Sega core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_sega",
 *   label = @Translation("EmulatorJS Sega"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSSegaFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'genesis_plus_gx';

    $settings['genesis_options'] = [
      'genesis_plus_gx_audio_eq_high' => "100",
      'genesis_plus_gx_audio_eq_low' => "100",
      'genesis_plus_gx_audio_eq_mid' => "100",
      'genesis_plus_gx_cdda_volume' => "100",
      'genesis_plus_gx_enhanced_vscroll_limit' => "8",
      'genesis_plus_gx_fm_preamp' => "100",
      'genesis_plus_gx_frameskip_threshold' => "33",
      'genesis_plus_gx_lowpass_range' => "60",
      'genesis_plus_gx_md_channel_0_volume' => "100",
      'genesis_plus_gx_md_channel_1_volume' => "100",
      'genesis_plus_gx_md_channel_2_volume' => "100",
      'genesis_plus_gx_md_channel_3_volume' => "100",
      'genesis_plus_gx_md_channel_4_volume' => "100",
      'genesis_plus_gx_md_channel_5_volume' => "100",
      'genesis_plus_gx_pcm_volume' => "100",
      'genesis_plus_gx_psg_channel_0_volume' => "100",
      'genesis_plus_gx_psg_channel_1_volume' => "100",
      'genesis_plus_gx_psg_channel_2_volume' => "100",
      'genesis_plus_gx_psg_channel_3_volume' => "100",
      'genesis_plus_gx_psg_preamp' => "150",
      'genesis_plus_gx_sms_fm_channel_0_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_1_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_2_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_3_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_4_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_5_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_6_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_7_volume' => "100",
      'genesis_plus_gx_sms_fm_channel_8_volume' => "100",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'genesis_plus_gx' => "genesis_plus_gx",
      // 'picodrive' => "picodrive",
      'smsplus' => "smsplus",
    ];

    // Genesis_plus_gx options.
    $genesis_options = $this->getSetting('genesis_options');
    $form['genesis_options'] = [
      '#type' => 'details',
      '#title' => 'genesis_plus_gx Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'genesis_plus_gx'],
        ],
      ],
    ];

    $zeroto100x10 = [];
    for ($i = 0; $i <= 100; $i += 10) {
      $zeroto100x10[strval($i)] = strval($i);
    }
    $zeroto100x5 = [];
    for ($i = 0; $i <= 100; $i += 5) {
      $zeroto100x5[strval($i)] = strval($i);
    }
    $zeroto200x5 = [];
    for ($i = 0; $i <= 200; $i += 5) {
      $zeroto200x5[strval($i)] = strval($i);
    }

    $form['genesis_options']['genesis_plus_gx_audio_eq_high'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_audio_eq_high',
      '#options' => $zeroto100x5,
      '#default_value' => $genesis_options['genesis_plus_gx_audio_eq_high'],
    ];

    $form['genesis_options']['genesis_plus_gx_audio_eq_low'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_audio_eq_low',
      '#options' => $zeroto100x5,
      '#default_value' => $genesis_options['genesis_plus_gx_audio_eq_low'],
    ];

    $form['genesis_options']['genesis_plus_gx_audio_eq_mid'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_audio_eq_mid',
      '#options' => $zeroto100x5,
      '#default_value' => $genesis_options['genesis_plus_gx_audio_eq_mid'],
    ];

    $form['genesis_options']['genesis_plus_gx_cdda_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_cdda_volume',
      '#options' => $zeroto100x5,
      '#default_value' => $genesis_options['genesis_plus_gx_cdda_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_enhanced_vscroll_limit'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_enhanced_vscroll_limit',
      '#options' => [],
      '#default_value' => $genesis_options['genesis_plus_gx_enhanced_vscroll_limit'],
    ];
    for ($i = 2; $i <= 16; $i += 2) {
      $form['genesis_options']['genesis_plus_gx_enhanced_vscroll_limit']['#options'][strval($i)] = strval($i);
    }

    $form['genesis_options']['genesis_plus_gx_fm_preamp'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_fm_preamp',
      '#options' => $zeroto200x5,
      '#default_value' => $genesis_options['genesis_plus_gx_fm_preamp'],
    ];

    $form['genesis_options']['genesis_plus_gx_frameskip_threshold'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_frameskip_threshold',
      '#options' => [],
      '#default_value' => $genesis_options['genesis_plus_gx_frameskip_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['genesis_options']['genesis_plus_gx_frameskip_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['genesis_options']['genesis_plus_gx_lowpass_range'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_lowpass_range',
      '#options' => [],
      '#default_value' => $genesis_options['genesis_plus_gx_lowpass_range'],
    ];
    for ($i = 5; $i <= 95; $i += 5) {
      $form['genesis_options']['genesis_plus_gx_lowpass_range']['#options'][strval($i)] = strval($i);
    }

    $form['genesis_options']['genesis_plus_gx_md_channel_0_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_0_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_0_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_md_channel_1_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_1_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_1_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_md_channel_2_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_2_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_2_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_md_channel_3_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_3_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_3_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_md_channel_4_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_4_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_4_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_md_channel_5_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_md_channel_5_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_md_channel_5_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_pcm_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_pcm_volume',
      '#options' => $zeroto100x5,
      '#default_value' => $genesis_options['genesis_plus_gx_pcm_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_psg_channel_0_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_psg_channel_0_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_psg_channel_0_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_psg_channel_1_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_psg_channel_1_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_psg_channel_1_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_psg_channel_2_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_psg_channel_2_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_psg_channel_2_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_psg_channel_3_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_psg_channel_3_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_psg_channel_3_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_psg_preamp'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_psg_preamp',
      '#options' => $zeroto200x5,
      '#default_value' => $genesis_options['genesis_plus_gx_psg_preamp'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_0_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_0_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_0_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_1_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_1_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_1_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_2_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_2_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_2_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_3_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_3_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_3_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_4_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_4_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_4_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_5_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_5_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_5_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_6_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_6_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_6_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_7_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_7_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_7_volume'],
    ];

    $form['genesis_options']['genesis_plus_gx_sms_fm_channel_8_volume'] = [
      '#type' => 'select',
      '#title' => 'genesis_plus_gx_sms_fm_channel_8_volume',
      '#options' => $zeroto100x10,
      '#default_value' => $genesis_options['genesis_plus_gx_sms_fm_channel_8_volume'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-sega-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-sega ejs-sega-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_sega/sega'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'genesis_plus_gx':
          $genesis_options = $this->getSetting('genesis_options');

          foreach ($genesis_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
