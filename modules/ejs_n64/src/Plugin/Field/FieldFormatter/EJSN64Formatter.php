<?php

namespace Drupal\ejs_n64\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Nintendo 64 core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_nintendo_64",
 *   label = @Translation("EmulatorJS Nintendo 64"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSN64Formatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'mupen64plus_next';

    $settings['mupen64plus_options'] = [
      'mupen64plus-43screensize' => "640x480",
      'mupen64plus-BackgroundMode' => "OnePiece",
      'mupen64plus-BilinearMode' => "standard",
      'mupen64plus-CountPerOp' => "0",
      'mupen64plus-CountPerOpDenomPot' => "0",
      'mupen64plus-EnableCopyDepthToRDRAM' => "Software",
      'mupen64plus-FXAA' => "0",
      'mupen64plus-Framerate' => "Original",
      'mupen64plus-MaxHiResTxVramLimit' => "0",
      'mupen64plus-MaxTxCacheSize' => "8000",
      'mupen64plus-MultiSampling' => "0",
      'mupen64plus-OverscanBottom' => "0",
      'mupen64plus-OverscanLeft' => "0",
      'mupen64plus-OverscanRight' => "0",
      'mupen64plus-OverscanTop' => "0",
      'mupen64plus-astick-deadzone' => "15",
      'mupen64plus-astick-sensitivity' => "100",
      'mupen64plus-d-cbutton' => "C3",
      'mupen64plus-l-cbutton' => "C2",
      'mupen64plus-pak1' => "memory",
      'mupen64plus-pak2' => "none",
      'mupen64plus-pak3' => "none",
      'mupen64plus-pak4' => "none",
      'mupen64plus-r-cbutton' => "C1",
      'mupen64plus-txFilterMode' => "None",
      'mupen64plus-txEnhancementMode' => "None",
      'mupen64plus-u-cbutton' => "C4",
      'mupen64plus-virefresh' => "Auto",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'mupen64plus_next' => "mupen64plus_next",
    ];

    // Mupen64plus_next options.
    $mupen64plus_options = $this->getSetting('mupen64plus_options');
    $form['mupen64plus_options'] = [
      '#type' => 'details',
      '#title' => 'mupen64plus Options',
      '#weight' => -3,
    ];

    $form['mupen64plus_options']['mupen64plus-43screensize'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-43screensize',
      '#options' => [
        '320x240' => "320x240",
        '640x480' => "640x480",
        '960x720' => "960x720",
        '1280x960' => "1280x960",
        '1440x1080' => "1440x1080",
        '1600x1200' => "1600x1200",
        '1920x1440' => "1920x1440",
        '2240x1680' => "2240x1680",
        '2560x1920' => "2560x1920",
        '2880x2160' => "2880x2160",
        '3200x2400' => "3200x2400",
        '3520x2640' => "3520x2640",
        '3840x2880' => "3840x2880",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-43screensize'],
    ];

    $form['mupen64plus_options']['mupen64plus-BackgroundMode'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-BackgroundMode',
      '#options' => [
        'OnePiece' => "OnePiece",
        'Stripped' => "Stripped",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-BackgroundMode'],
    ];

    $form['mupen64plus_options']['mupen64plus-BilinearMode'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-BilinearMode',
      '#options' => [
        '3point' => "3point",
        'standard' => "standard",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-BilinearMode'],
    ];

    $form['mupen64plus_options']['mupen64plus-CountPerOp'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-CountPerOp',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-CountPerOp'],
    ];
    for ($i = 0; $i <= 5; $i++) {
      $form['mupen64plus_options']['mupen64plus-CountPerOp']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-CountPerOpDenomPot'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-CountPerOpDenomPot',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-CountPerOpDenomPot'],
    ];
    for ($i = 0; $i <= 11; $i++) {
      $form['mupen64plus_options']['mupen64plus-CountPerOpDenomPot']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-EnableCopyDepthToRDRAM'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-EnableCopyDepthToRDRAM',
      '#options' => [
        'FromMem' => "FromMem",
        'Software' => "Software",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-EnableCopyDepthToRDRAM'],
    ];

    $form['mupen64plus_options']['mupen64plus-FXAA'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-FXAA',
      '#options' => [
        '0' => "0",
        '1' => "1",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-FXAA'],
    ];

    $form['mupen64plus_options']['mupen64plus-Framerate'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-Framerate',
      '#options' => [
        'Fullspeed' => "Fullspeed",
        'Original' => "Original",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-Framerate'],
    ];

    $form['mupen64plus_options']['mupen64plus-MaxHiResTxVramLimit'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-MaxHiResTxVramLimit',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-MaxHiResTxVramLimit'],
    ];
    for ($i = 0; $i <= 4000; $i += 500) {
      $form['mupen64plus_options']['mupen64plus-MaxHiResTxVramLimit']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-MaxTxCacheSize'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-MaxTxCacheSize',
      '#options' => [
        '1500' => "1500",
        '4000' => "4000",
        '8000' => "8000",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-MaxTxCacheSize'],
    ];

    $form['mupen64plus_options']['mupen64plus-MultiSampling'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-MultiSampling',
      '#options' => [
        '0' => "0",
        '2' => "2",
        '4' => "4",
        '8' => "8",
        '16' => "16",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-MultiSampling'],
    ];

    $form['mupen64plus_options']['mupen64plus-OverscanBottom'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-OverscanBottom',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-OverscanBottom'],
    ];
    for ($i = 0; $i <= 50; $i++) {
      $form['mupen64plus_options']['mupen64plus-OverscanBottom']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-OverscanLeft'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-OverscanLeft',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-OverscanLeft'],
    ];
    for ($i = 0; $i <= 50; $i++) {
      $form['mupen64plus_options']['mupen64plus-OverscanLeft']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-OverscanRight'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-OverscanRight',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-OverscanRight'],
    ];
    for ($i = 0; $i <= 50; $i++) {
      $form['mupen64plus_options']['mupen64plus-OverscanRight']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-OverscanTop'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-OverscanTop',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-OverscanTop'],
    ];
    for ($i = 0; $i <= 50; $i++) {
      $form['mupen64plus_options']['mupen64plus-OverscanTop']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-astick-deadzone'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-astick-deadzone',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-astick-deadzone'],
    ];
    for ($i = 0; $i <= 30; $i += 5) {
      $form['mupen64plus_options']['mupen64plus-astick-deadzone']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-astick-sensitivity'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-astick-sensitivity',
      '#options' => [],
      '#default_value' => $mupen64plus_options['mupen64plus-astick-sensitivity'],
    ];
    for ($i = 50; $i <= 150; $i += 5) {
      $form['mupen64plus_options']['mupen64plus-astick-sensitivity']['#options'][strval($i)] = strval($i);
    }

    $form['mupen64plus_options']['mupen64plus-d-cbutton'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-d-cbutton',
      '#options' => [
        'C1' => "C1",
        'C2' => "C2",
        'C3' => "C3",
        'C4' => "C4",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-d-cbutton'],
    ];

    $form['mupen64plus_options']['mupen64plus-l-cbutton'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-l-cbutton',
      '#options' => [
        'C1' => "C1",
        'C2' => "C2",
        'C3' => "C3",
        'C4' => "C4",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-l-cbutton'],
    ];

    $form['mupen64plus_options']['mupen64plus-pak1'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-pak1',
      '#options' => [
        'none' => "none",
        'memory' => "memory",
        'rumble' => "rumble",
        'transfer' => "transfer",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-pak1'],
    ];

    $form['mupen64plus_options']['mupen64plus-pak2'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-pak2',
      '#options' => [
        'none' => "none",
        'memory' => "memory",
        'rumble' => "rumble",
        'transfer' => "transfer",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-pak2'],
    ];

    $form['mupen64plus_options']['mupen64plus-pak3'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-pak3',
      '#options' => [
        'none' => "none",
        'memory' => "memory",
        'rumble' => "rumble",
        'transfer' => "transfer",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-pak3'],
    ];

    $form['mupen64plus_options']['mupen64plus-pak4'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-pak4',
      '#options' => [
        'none' => "none",
        'memory' => "memory",
        'rumble' => "rumble",
        'transfer' => "transfer",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-pak4'],
    ];

    $form['mupen64plus_options']['mupen64plus-r-cbutton'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-r-cbutton',
      '#options' => [
        'C1' => "C1",
        'C2' => "C2",
        'C3' => "C3",
        'C4' => "C4",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-r-cbutton'],
    ];

    $form['mupen64plus_options']['mupen64plus-txEnhancementMode'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-txEnhancementMode',
      '#options' => [
        '2xBRZ' => "2xBRZ",
        '3xBRZ' => "3xBRZ",
        '4xBRZ' => "4xBRZ",
        '5xBRZ' => "5xBRZ",
        '6xBRZ' => "6xBRZ",
        'As Is' => "As Is",
        'HQ2X' => "HQ2X",
        'HQ2XS' => "HQ2XS",
        'HQ4X' => "HQ4X",
        'LQ2X' => "LQ2X",
        'LQ2XS' => "LQ2XS",
        'None' => "None",
        'X2' => "X2",
        'X2SAI' => "X2SAI",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-txEnhancementMode'],
    ];

    $form['mupen64plus_options']['mupen64plus-txFilterMode'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-txFilterMode',
      '#options' => [
        'None' => "None",
        'Sharp filtering 1' => "Sharp filtering 1",
        'Sharp filtering 2' => "Sharp filtering 2",
        'Smooth filtering 1' => "Smooth filtering 1",
        'Smooth filtering 2' => "Smooth filtering 2",
        'Smooth filtering 3' => "Smooth filtering 3",
        'Smooth filtering 4' => "Smooth filtering 4",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-txFilterMode'],
    ];

    $form['mupen64plus_options']['mupen64plus-u-cbutton'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-u-cbutton',
      '#options' => [
        'C1' => "C1",
        'C2' => "C2",
        'C3' => "C3",
        'C4' => "C4",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-u-cbutton'],
    ];

    $form['mupen64plus_options']['mupen64plus-virefresh'] = [
      '#type' => 'select',
      '#title' => 'mupen64plus-virefresh',
      '#options' => [
        '1500' => "1500",
        '2200' => "2200",
        'Auto' => "Auto",
      ],
      '#default_value' => $mupen64plus_options['mupen64plus-virefresh'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-n64-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-n64 ejs-n64-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_n64/n64'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'mupen64plus_next':
          $mupen64plus_options = $this->getSetting('mupen64plus_options');

          foreach ($mupen64plus_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
