<?php

namespace Drupal\ejs_nes\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS NES/Famicom core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_nintendo_entertainment_system",
 *   label = @Translation("EmulatorJS Nintendo Entertainment System/Famicom"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSNESFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'fceumm';
    $settings['core_options']['EJS_lightgun'] = FALSE;

    $settings['fceumm_options'] = [
      'fceumm_aspect' => '8:7 PAR',
      'fceumm_mouse_sensitivity' => '100',
      'fceumm_overscan_v_top' => 0,
      'fceumm_overscan_v_bottom' => 0,
      'fceumm_overscan_h_left' => 0,
      'fceumm_overscan_h_right' => 0,
      'fceumm_region' => 'Auto',
      'fceumm_sndquality' => 'Low',
      'fceumm_turbo_delay' => '3',
      'fceumm_turbo_enable' => 'None',
      'fceumm_zapper_tolerance' => '6',
    ];

    $settings['nestopia_options'] = [
      'nestopia_audio_type' => 'stereo',
      'nestopia_audio_vol_dpcm' => '100',
      'nestopia_audio_vol_fds' => '100',
      'nestopia_audio_vol_mmc5' => '100',
      'nestopia_audio_vol_n163' => '100',
      'nestopia_audio_vol_noise' => '100',
      'nestopia_audio_vol_s5b' => '100',
      'nestopia_audio_vol_sq1' => '100',
      'nestopia_audio_vol_sq2' => '100',
      'nestopia_audio_vol_tri' => '100',
      'nestopia_audio_vol_vrc6' => '100',
      'nestopia_audio_vol_vrc7' => '100',
      'nestopia_overclock' => '1x',
      'nestopia_overscan_v_top' => 0,
      'nestopia_overscan_v_bottom' => 0,
      'nestopia_overscan_h_left' => 0,
      'nestopia_overscan_h_right' => 0,
      'nestopia_ram_power_state' => '0x00',
      'nestopia_turbo_pulse' => '2',
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'fceumm' => 'fceumm',
      'nestopia' => 'nestopia',
    ];

    $form['core_options']['EJS_lightgun'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_lightgun',
      '#weight' => -1,
      '#default_value' => $core_options['EJS_lightgun'],
    ];

    // Fceumm options.
    $fceumm_options = $this->getSetting('fceumm_options');
    $form['fceumm_options'] = [
      '#type' => 'details',
      '#title' => 'fceumm Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'fceumm'],
        ],
      ],
    ];

    $form['fceumm_options']['fceumm_aspect'] = [
      '#type' => 'select',
      '#title' => 'fceumm_aspect',
      '#options' => [
        '8:7 PAR' => '8:7 PAR',
        '4:3' => '4:3',
      ],
      '#default_value' => $fceumm_options['fceumm_aspect'],
    ];

    $form['fceumm_options']['fceumm_mouse_sensitivity'] = [
      '#type' => 'select',
      '#title' => 'fceumm_mouse_sensitivity',
      '#options' => [],
      '#default_value' => $fceumm_options['fceumm_mouse_sensitivity'],
    ];
    for ($i = 20; $i <= 200; $i += 10) {
      $form['fceumm_options']['fceumm_mouse_sensitivity']['#options'][strval($i)] = strval($i);
    }

    $form['fceumm_options']['fceumm_overscan_h_left'] = [
      '#type' => 'select',
      '#title' => 'fceumm_overscan_h_left',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
      ],
      '#default_value' => $fceumm_options['fceumm_overscan_h_left'],
    ];

    $form['fceumm_options']['fceumm_overscan_h_right'] = [
      '#type' => 'select',
      '#title' => 'fceumm_overscan_h_right',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
      ],
      '#default_value' => $fceumm_options['fceumm_overscan_h_right'],
    ];

    $form['fceumm_options']['fceumm_overscan_v_bottom'] = [
      '#type' => 'select',
      '#title' => 'fceumm_overscan_v_bottom',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
        '20' => '20',
        '24' => '24',
      ],
      '#default_value' => $fceumm_options['fceumm_overscan_v_bottom'],
    ];

    $form['fceumm_options']['fceumm_overscan_v_top'] = [
      '#type' => 'select',
      '#title' => 'fceumm_overscan_v_top',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
        '20' => '20',
        '24' => '24',
      ],
      '#default_value' => $fceumm_options['fceumm_overscan_v_top'],
    ];

    $form['fceumm_options']['fceumm_region'] = [
      '#type' => 'select',
      '#title' => 'fceumm_region',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        'Auto' => 'Auto',
        'NTSC' => 'NTSC',
        'PAL' => 'PAL',
        'Dendy' => 'Dendy',
      ],
      '#default_value' => $fceumm_options['fceumm_region'],
    ];

    $form['fceumm_options']['fceumm_sndquality'] = [
      '#type' => 'select',
      '#title' => 'fceumm_sndquality',
      '#options' => [
        'Low' => 'Low',
        'High' => 'High',
        'Very High' => 'Very High',
      ],
      '#default_value' => $fceumm_options['fceumm_sndquality'],
    ];

    $form['fceumm_options']['fceumm_turbo_delay'] = [
      '#type' => 'select',
      '#title' => 'fceumm_turbo_delay',
      '#options' => [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '5' => '5',
        '10' => '10',
        '15' => '15',
        '30' => '30',
        '60' => '60',
      ],
      '#default_value' => $fceumm_options['fceumm_turbo_delay'],
    ];

    $form['fceumm_options']['fceumm_turbo_enable'] = [
      '#type' => 'select',
      '#title' => 'fceumm_turbo_enable',
      '#options' => [
        'None' => 'None',
        'Player 1' => 'Player 1',
        'Player 2' => 'Player 2',
        'Both' => 'Both',
      ],
      '#default_value' => $fceumm_options['fceumm_turbo_enable'],
    ];

    $form['fceumm_options']['fceumm_zapper_tolerance'] = [
      '#type' => 'select',
      '#title' => 'fceumm_zapper_tolerance',
      '#options' => [],
      '#default_value' => $fceumm_options['fceumm_zapper_tolerance'],
    ];
    for ($i = 0; $i <= 20; $i++) {
      $form['fceumm_options']['fceumm_zapper_tolerance']['#options'][strval($i)] = strval($i);
    }

    // Nestopia options.
    $nestopia_options = $this->getSetting('nestopia_options');
    $form['nestopia_options'] = [
      '#type' => 'details',
      '#title' => 'nestopia Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'nestopia'],
        ],
      ],
    ];

    $form['nestopia_options']['nestopia_audio_type'] = [
      '#type' => 'select',
      '#title' => 'nestopia_audio_type',
      '#options' => [
        'mono' => 'mono',
        'stereo' => 'stereo',
      ],
      '#default_value' => $nestopia_options['nestopia_audio_type'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_dpcm'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_dpcm',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_dpcm'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_fds'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_fds',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_fds'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_mmc5'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_mmc5',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_mmc5'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_n163'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_n163',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_n163'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_noise'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_noise',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_noise'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_s5b'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_s5b',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_s5b'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_sq1'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_sq1',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_sq1'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_sq2'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_sq2',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_sq2'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_tri'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_tri',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_tri'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_vrc6'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_vrc6',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_vrc6'],
    ];

    $form['nestopia_options']['nestopia_audio_vol_vrc7'] = [
      '#type' => 'range',
      '#title' => 'nestopia_audio_vol_vrc7',
      '#min' => 0,
      '#max' => 100,
      '#step' => 10,
      '#default_value' => $nestopia_options['nestopia_audio_vol_vrc7'],
    ];

    $form['nestopia_options']['nestopia_overclock'] = [
      '#type' => 'select',
      '#title' => 'nestopia_overclock',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '1x' => '1x',
        '2x' => '2x',
      ],
      '#default_value' => $nestopia_options['nestopia_overclock'],
    ];

    $form['nestopia_options']['nestopia_overscan_h_left'] = [
      '#type' => 'select',
      '#title' => 'nestopia_overscan_h_left',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
      ],
      '#default_value' => $nestopia_options['nestopia_overscan_h_left'],
    ];

    $form['nestopia_options']['nestopia_overscan_h_right'] = [
      '#type' => 'select',
      '#title' => 'nestopia_overscan_h_right',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
      ],
      '#default_value' => $nestopia_options['nestopia_overscan_h_right'],
    ];

    $form['nestopia_options']['nestopia_overscan_v_bottom'] = [
      '#type' => 'select',
      '#title' => 'nestopia_overscan_v_bottom',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
        '20' => '20',
        '24' => '24',
      ],
      '#default_value' => $nestopia_options['nestopia_overscan_v_bottom'],
    ];

    $form['nestopia_options']['nestopia_overscan_v_top'] = [
      '#type' => 'select',
      '#title' => 'nestopia_overscan_v_top',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '0' => '0',
        '4' => '4',
        '8' => '8',
        '12' => '12',
        '16' => '16',
        '20' => '20',
        '24' => '24',
      ],
      '#default_value' => $nestopia_options['nestopia_overscan_v_top'],
    ];

    $form['nestopia_options']['nestopia_ram_power_state'] = [
      '#type' => 'select',
      '#title' => 'nestopia_ram_power_state',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '0x00' => '0x00',
        '0xFF' => '0xFF',
      ],
      '#default_value' => $nestopia_options['nestopia_ram_power_state'],
    ];

    $form['nestopia_options']['nestopia_turbo_pulse'] = [
      '#type' => 'select',
      '#title' => 'nestopia_turbo_pulse',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
      ],
      '#default_value' => $nestopia_options['nestopia_turbo_pulse'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-nes-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-nes ejs-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_nes/nes'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'fceumm':
          $fceumm_options = $this->getSetting('fceumm_options');

          foreach ($fceumm_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'nestopia':
          $nestopia_options = $this->getSetting('nestopia_options');

          foreach ($nestopia_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }
    }

    return $elements;
  }

}
