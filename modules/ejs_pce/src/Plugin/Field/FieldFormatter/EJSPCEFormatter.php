<?php

namespace Drupal\ejs_pce\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS PC Engine core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_pc_engine",
 *   label = @Translation("EmulatorJS PC Engine"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSPCEFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'mednafen_pce';

    $settings['mednafen_options'] = [
      'pce_adpcmextraprec' => "10-bit",
      'pce_adpcmvolume' => "100",
      'pce_cdbios' => "System Card 3",
      'pce_cddavolume' => "100",
      'pce_cdpsgvolume' => "100",
      'pce_cdspeed' => "1",
      'pce_default_joypad_type_p1' => "2 Buttons",
      'pce_default_joypad_type_p2' => "2 Buttons",
      'pce_default_joypad_type_p3' => "2 Buttons",
      'pce_default_joypad_type_p4' => "2 Buttons",
      'pce_default_joypad_type_p5' => "2 Buttons",
      'pce_mouse_sensitivity' => "1.25",
      'pce_ocmultiplier' => "1",
      'pce_palette' => "RGB",
      'pce_psgrevision' => "HuC6280A",
      'pce_Turbo_Delay' => "Fast",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'mednafen_pce' => "mednafen_pce",
    ];

    // Mednafen_pce options.
    $mednafen_options = $this->getSetting('mednafen_options');
    $form['mednafen_options'] = [
      '#type' => 'details',
      '#title' => 'mednafen_pce Options',
      '#weight' => -3,
    ];

    $form['mednafen_options']['pce_Turbo_Delay'] = [
      '#type' => 'select',
      '#title' => 'pce_Turbo_Delay',
      '#options' => [
        'Fast' => "Fast",
        'Medium' => "Medium",
        'Slow' => "Slow",
      ],
      '#default_value' => $mednafen_options['pce_Turbo_Delay'],
    ];

    $form['mednafen_options']['pce_adpcmextraprec'] = [
      '#type' => 'select',
      '#title' => 'pce_adpcmextraprec',
      '#options' => [
        '10-bit' => "10-bit",
        '12-bit' => "12-bit",
      ],
      '#default_value' => $mednafen_options['pce_adpcmextraprec'],
    ];

    $form['mednafen_options']['pce_adpcmvolume'] = [
      '#type' => 'select',
      '#title' => 'pce_adpcmvolume',
      '#options' => [],
      '#default_value' => $mednafen_options['pce_adpcmvolume'],
    ];
    for ($i = 0; $i <= 200; $i += 10) {
      $form['mednafen_options']['pce_adpcmvolume']['#options'][strval($i)] = strval($i);
    }

    $form['mednafen_options']['pce_cdbios'] = [
      '#type' => 'select',
      '#title' => 'pce_cdbios',
      '#options' => [
        'Games Express' => "Games Express",
        'System Card 1' => "System Card 1",
        'System Card 2' => "System Card 2",
        'System Card 2 US' => "System Card 2 US",
        'System Card 3' => "System Card 3",
        'System Card 3 US' => "System Card 3 US",
      ],
      '#default_value' => $mednafen_options['pce_cdbios'],
    ];

    $form['mednafen_options']['pce_cddavolume'] = [
      '#type' => 'select',
      '#title' => 'pce_cddavolume',
      '#options' => [],
      '#default_value' => $mednafen_options['pce_cddavolume'],
    ];
    for ($i = 0; $i <= 200; $i += 10) {
      $form['mednafen_options']['pce_cddavolume']['#options'][strval($i)] = strval($i);
    }

    $form['mednafen_options']['pce_cdpsgvolume'] = [
      '#type' => 'select',
      '#title' => 'pce_cdpsgvolume',
      '#options' => [],
      '#default_value' => $mednafen_options['pce_cdpsgvolume'],
    ];
    for ($i = 0; $i <= 200; $i += 10) {
      $form['mednafen_options']['pce_cdpsgvolume']['#options'][strval($i)] = strval($i);
    }

    $form['mednafen_options']['pce_cdspeed'] = [
      '#type' => 'select',
      '#title' => 'pce_cdspeed',
      '#options' => [
        '1' => "1",
        '2' => "2",
        '4' => "4",
        '8' => "8",
      ],
      '#default_value' => $mednafen_options['pce_cdspeed'],
    ];

    $form['mednafen_options']['pce_default_joypad_type_p1'] = [
      '#type' => 'select',
      '#title' => 'pce_default_joypad_type_p1',
      '#options' => [
        '2 Buttons' => "2 Buttons",
        '6 Buttons' => "6 Buttons",
      ],
      '#default_value' => $mednafen_options['pce_default_joypad_type_p1'],
    ];

    $form['mednafen_options']['pce_default_joypad_type_p2'] = [
      '#type' => 'select',
      '#title' => 'pce_default_joypad_type_p2',
      '#options' => [
        '2 Buttons' => "2 Buttons",
        '6 Buttons' => "6 Buttons",
      ],
      '#default_value' => $mednafen_options['pce_default_joypad_type_p2'],
    ];

    $form['mednafen_options']['pce_default_joypad_type_p3'] = [
      '#type' => 'select',
      '#title' => 'pce_default_joypad_type_p3',
      '#options' => [
        '2 Buttons' => "2 Buttons",
        '6 Buttons' => "6 Buttons",
      ],
      '#default_value' => $mednafen_options['pce_default_joypad_type_p3'],
    ];

    $form['mednafen_options']['pce_default_joypad_type_p4'] = [
      '#type' => 'select',
      '#title' => 'pce_default_joypad_type_p4',
      '#options' => [
        '2 Buttons' => "2 Buttons",
        '6 Buttons' => "6 Buttons",
      ],
      '#default_value' => $mednafen_options['pce_default_joypad_type_p4'],
    ];

    $form['mednafen_options']['pce_default_joypad_type_p5'] = [
      '#type' => 'select',
      '#title' => 'pce_default_joypad_type_p5',
      '#options' => [
        '2 Buttons' => "2 Buttons",
        '6 Buttons' => "6 Buttons",
      ],
      '#default_value' => $mednafen_options['pce_default_joypad_type_p5'],
    ];

    $form['mednafen_options']['pce_mouse_sensitivity'] = [
      '#type' => 'select',
      '#title' => 'pce_mouse_sensitivity',
      '#options' => [],
      '#default_value' => $mednafen_options['pce_mouse_sensitivity'],
    ];
    for ($i = 0.125; $i <= 1; $i += 0.125) {
      $form['mednafen_options']['pce_mouse_sensitivity']['#options'][strval(number_format($i, 3, '.'))] = strval(number_format($i, 3, '.'));
    }
    for ($i = 1.25; $i <= 5; $i += 0.25) {
      $form['mednafen_options']['pce_mouse_sensitivity']['#options'][strval(number_format($i, 2, '.'))] = strval(number_format($i, 2, '.'));
    }

    $form['mednafen_options']['pce_ocmultiplier'] = [
      '#type' => 'select',
      '#title' => 'pce_ocmultiplier',
      '#options' => [],
      '#default_value' => $mednafen_options['pce_ocmultiplier'],
    ];
    for ($i = 1; $i <= 9; $i++) {
      $form['mednafen_options']['pce_ocmultiplier']['#options'][strval($i)] = strval($i);
    }
    for ($i = 10; $i <= 50; $i += 10) {
      $form['mednafen_options']['pce_ocmultiplier']['#options'][strval($i)] = strval($i);
    }

    $form['mednafen_options']['pce_palette'] = [
      '#type' => 'select',
      '#title' => 'pce_palette',
      '#options' => [
        'Composite' => "Composite",
        'RGB' => "RGB",
      ],
      '#default_value' => $mednafen_options['pce_palette'],
    ];

    $form['mednafen_options']['pce_psgrevision'] = [
      '#type' => 'select',
      '#title' => 'pce_psgrevision',
      '#options' => [
        'HuC6280' => "HuC6280",
        'HuC6280A' => "HuC6280A",
      ],
      '#default_value' => $mednafen_options['pce_psgrevision'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-pce-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-pce ejs-pce-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_pce/pce'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'mednafen_pce':
          $mednafen_options = $this->getSetting('mednafen_options');

          foreach ($mednafen_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
