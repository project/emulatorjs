<?php

namespace Drupal\ejs_arcade\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Arcade core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_arcade",
 *   label = @Translation("EmulatorJS Arcade"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSArcadeFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'fbneo';

    $settings['fbneo_options'] = [
      'fbneo_analog_speed' => "100%",
      'fbneo_cpu_speed_adjust' => "100%",
      'fbneo_diagnostic_input' => "Hold Start",
      'fbneo_frameskip_manual_threshold' => "33",
      'fbneo_lightgun_crosshair_emulation' => "hide with lightgun device",
      'fbneo_sample_interpolation' => "4-point 3rd order",
      'fbneo_samplerate' => "48000",
    ];

    $settings['fba2012cps1_options'] = [
      'fba2012cps1_aspect' => "DAR",
      'fba2012cps1_cpu_speed_adjust' => "100",
      'fba2012cps1_frameskip_threshold' => "33",
      'fba2012cps1_lowpass_range' => "60",
    ];

    $settings['fba2012cps2_options'] = [
      'fba2012cps2_aspect' => "DAR",
      'fba2012cps2_cpu_speed_adjust' => "100",
      'fba2012cps2_frameskip_threshold' => "33",
      'fba2012cps2_lowpass_range' => "60",
    ];

    $settings['mame2003_options'] = [
      'mame2003_art_resolution' => "1",
      'mame2003_autosave_hiscore' => "default",
      'mame2003_brightness' => "1.0",
      'mame2003_gamma' => "1.0",
      'mame2003_input_interface' => "simultaneous",
      'mame2003_mouse_device' => "mouse",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'fbneo' => "fbneo",
      'fbalpha2012_cps1' => "fbalpha2012_cps1",
      'fbalpha2012_cps2' => "fbalpha2012_cps2",
      'mame2003' => "mame2003",
    ];

    // Fbneo options.
    $fbneo_options = $this->getSetting('fbneo_options');
    $form['fbneo_options'] = [
      '#type' => 'details',
      '#title' => 'fbneo Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'fbneo'],
        ],
      ],
    ];

    $form['fbneo_options']['fbneo_analog_speed'] = [
      '#type' => 'select',
      '#title' => 'fbneo-analog-speed',
      '#options' => [],
      '#default_value' => $fbneo_options['fbneo_analog_speed'],
    ];
    for ($i = 25; $i <= 99; $i++) {
      $form['fbneo_options']['fbneo_analog_speed']['#options'][strval($i) . "%"] = strval($i) . "%";
    }
    for ($i = 100; $i <= 199; $i += 5) {
      $form['fbneo_options']['fbneo_analog_speed']['#options'][strval($i) . "%"] = strval($i) . "%";
    }
    for ($i = 200; $i <= 400; $i += 10) {
      $form['fbneo_options']['fbneo_analog_speed']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['fbneo_options']['fbneo_cpu_speed_adjust'] = [
      '#type' => 'select',
      '#title' => 'fbneo-cpu-speed-adjust',
      '#options' => [],
      '#default_value' => $fbneo_options['fbneo_cpu_speed_adjust'],
    ];
    for ($i = 25; $i <= 99; $i++) {
      $form['fbneo_options']['fbneo_cpu_speed_adjust']['#options'][strval($i) . "%"] = strval($i) . "%";
    }
    for ($i = 100; $i <= 199; $i += 5) {
      $form['fbneo_options']['fbneo_cpu_speed_adjust']['#options'][strval($i) . "%"] = strval($i) . "%";
    }
    for ($i = 200; $i <= 400; $i += 10) {
      $form['fbneo_options']['fbneo_cpu_speed_adjust']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['fbneo_options']['fbneo_diagnostic_input'] = [
      '#type' => 'select',
      '#title' => 'fbneo-diagnostic-input',
      '#options' => [
        'Hold Select' => "Hold Select",
        'Hold Select + A + B' => "Hold Select + A + B",
        'Hold Select + L + R' => "Hold Select + L + R",
        'Hold Start' => "Hold Start",
        'Hold Start + A + B' => "Hold Start + A + B",
        'Hold Start + L + R' => "Hold Start + L + R",
        'None' => "None",
        'Select + A + B' => "Select + A + B",
        'Select + L + R' => "Select + L + R",
        'Start + A + B' => "Start + A + B",
        'Start + L + R' => "Start + L + R",
      ],
      '#default_value' => $fbneo_options['fbneo_diagnostic_input'],
    ];

    $form['fbneo_options']['fbneo_frameskip_manual_threshold'] = [
      '#type' => 'select',
      '#title' => 'fbneo-frameskip-manual-threshold',
      '#options' => [],
      '#default_value' => $fbneo_options['fbneo_frameskip_manual_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['fbneo_options']['fbneo_frameskip_manual_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['fbneo_options']['fbneo_lightgun_crosshair_emulation'] = [
      '#type' => 'select',
      '#title' => 'fbneo-lightgun-crosshair-emulation',
      '#options' => [
        'always hide' => "always hide",
        'always show' => "always show",
        'hide with lightgun device' => "hide with lightgun device",
      ],
      '#default_value' => $fbneo_options['fbneo_lightgun_crosshair_emulation'],
    ];

    $form['fbneo_options']['fbneo_sample_interpolation'] = [
      '#type' => 'select',
      '#title' => 'fbneo-sample-interpolation',
      '#options' => [
        '2-point 1st order' => "2-point 1st order",
        '4-point 3rd order' => "4-point 3rd order",
      ],
      '#default_value' => $fbneo_options['fbneo_sample_interpolation'],
    ];

    $form['fbneo_options']['fbneo_samplerate'] = [
      '#type' => 'select',
      '#title' => 'fbneo-samplerate',
      '#options' => [
        '44100' => "44100",
        '48000' => "48000",
      ],
      '#default_value' => $fbneo_options['fbneo_samplerate'],
    ];

    // Fbalpha2012_cps1 options.
    $fba2012cps1_options = $this->getSetting('fba2012cps1_options');
    $form['fba2012cps1_options'] = [
      '#type' => 'details',
      '#title' => 'fbalpha2012_cps1 Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'fbalpha2012_cps1'],
        ],
      ],
    ];

    $form['fba2012cps1_options']['fba2012cps1_aspect'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps1_aspect',
      '#options' => [
        'DAR' => "DAR",
        'PAR' => "PAR",
      ],
      '#default_value' => $fba2012cps1_options['fba2012cps1_aspect'],
    ];

    $form['fba2012cps1_options']['fba2012cps1_cpu_speed_adjust'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps1_cpu_speed_adjust',
      '#options' => [],
      '#default_value' => $fba2012cps1_options['fba2012cps1_cpu_speed_adjust'],
    ];
    for ($i = 100; $i <= 200; $i += 10) {
      $form['fba2012cps1_options']['fba2012cps1_cpu_speed_adjust']['#options'][strval($i)] = strval($i);
    }

    $form['fba2012cps1_options']['fba2012cps1_frameskip_threshold'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps1_frameskip_threshold',
      '#options' => [],
      '#default_value' => $fba2012cps1_options['fba2012cps1_frameskip_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['fba2012cps1_options']['fba2012cps1_frameskip_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['fba2012cps1_options']['fba2012cps1_lowpass_range'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps1_lowpass_range',
      '#options' => [],
      '#default_value' => $fba2012cps1_options['fba2012cps1_lowpass_range'],
    ];
    for ($i = 5; $i <= 95; $i += 5) {
      $form['fba2012cps1_options']['fba2012cps1_lowpass_range']['#options'][strval($i)] = strval($i);
    }

    // Fbalpha2012_cps2 options.
    $fba2012cps2_options = $this->getSetting('fba2012cps2_options');
    $form['fba2012cps2_options'] = [
      '#type' => 'details',
      '#title' => 'fbalpha2012_cps2 Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'fbalpha2012_cps2'],
        ],
      ],
    ];

    $form['fba2012cps2_options']['fba2012cps2_aspect'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps2_aspect',
      '#options' => [
        'DAR' => "DAR",
        'PAR' => "PAR",
      ],
      '#default_value' => $fba2012cps2_options['fba2012cps2_aspect'],
    ];

    $form['fba2012cps2_options']['fba2012cps2_cpu_speed_adjust'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps2_cpu_speed_adjust',
      '#options' => [],
      '#default_value' => $fba2012cps2_options['fba2012cps2_cpu_speed_adjust'],
    ];
    for ($i = 100; $i <= 200; $i += 10) {
      $form['fba2012cps2_options']['fba2012cps2_cpu_speed_adjust']['#options'][strval($i)] = strval($i);
    }

    $form['fba2012cps2_options']['fba2012cps2_frameskip_threshold'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps2_frameskip_threshold',
      '#options' => [],
      '#default_value' => $fba2012cps2_options['fba2012cps2_frameskip_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['fba2012cps2_options']['fba2012cps2_frameskip_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['fba2012cps2_options']['fba2012cps2_lowpass_range'] = [
      '#type' => 'select',
      '#title' => 'fba2012cps2_lowpass_range',
      '#options' => [],
      '#default_value' => $fba2012cps2_options['fba2012cps2_lowpass_range'],
    ];
    for ($i = 5; $i <= 95; $i += 5) {
      $form['fba2012cps2_options']['fba2012cps2_lowpass_range']['#options'][strval($i)] = strval($i);
    }

    // Mame2003 options.
    $mame2003_options = $this->getSetting('mame2003_options');
    $form['mame2003_options'] = [
      '#type' => 'details',
      '#title' => 'mame2003 Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'mame2003'],
        ],
      ],
    ];

    $form['mame2003_options']['mame2003_art_resolution'] = [
      '#type' => 'select',
      '#title' => 'mame2003_art_resolution',
      '#options' => [
        '1' => "1",
        '2' => "2",
      ],
      '#default_value' => $mame2003_options['mame2003_art_resolution'],
    ];

    $form['mame2003_options']['mame2003_autosave_hiscore'] = [
      '#type' => 'select',
      '#title' => 'mame2003_autosave_hiscore',
      '#options' => [
        'default' => "default",
        'recursively' => "recursively",
      ],
      '#default_value' => $mame2003_options['mame2003_autosave_hiscore'],
    ];

    $form['mame2003_options']['mame2003_brightness'] = [
      '#type' => 'select',
      '#title' => 'mame2003_brightness',
      '#options' => [],
      '#default_value' => $mame2003_options['mame2003_brightness'],
    ];
    for ($i = 0.2; $i < 2.1; $i += 0.1) {
      $form['mame2003_options']['mame2003_brightness']['#options'][strval(number_format($i, 1, '.'))] = strval(number_format($i, 1, '.'));
    }

    $form['mame2003_options']['mame2003_gamma'] = [
      '#type' => 'select',
      '#title' => 'mame2003_gamma',
      '#options' => [],
      '#default_value' => $mame2003_options['mame2003_gamma'],
    ];
    for ($i = 0.5; $i < 2.1; $i += 0.1) {
      $form['mame2003_options']['mame2003_gamma']['#options'][strval(number_format($i, 1, '.'))] = strval(number_format($i, 1, '.'));
    }

    $form['mame2003_options']['mame2003_input_interface'] = [
      '#type' => 'select',
      '#title' => 'mame2003_input_interface',
      '#options' => [
        'keyboard' => "keyboard",
        'retropad' => "retropad",
        'simultaneous' => "simultaneous",
      ],
      '#default_value' => $mame2003_options['mame2003_input_interface'],
    ];

    $form['mame2003_options']['mame2003_mouse_device'] = [
      '#type' => 'select',
      '#title' => 'mame2003_mouse_device',
      '#options' => [
        'mouse' => "mouse",
        'pointer' => "pointer",
      ],
      '#default_value' => $mame2003_options['mame2003_mouse_device'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-arcade-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-arcade ejs-arcade-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          // 'library' => ['ejs_arcade/arcade'],
          'library' => ['emulatorjs/emulatorjs', 'ejs_arcade/arcade'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'fbneo':
          $fbneo_options = $this->getSetting('fbneo_options');

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-analog-speed'] = $fbneo_options['fbneo_analog_speed'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-cpu-speed-adjust'] = $fbneo_options['fbneo_cpu_speed_adjust'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-diagnostic-input'] = $fbneo_options['fbneo_diagnostic_input'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-frameskip-manual-threshold'] = $fbneo_options['fbneo_frameskip_manual_threshold'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-lightgun-crosshair-emulation'] = $fbneo_options['fbneo_lightgun_crosshair_emulation'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-sample-interpolation'] = $fbneo_options['fbneo_sample_interpolation'];

          $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions']['fbneo-samplerate'] = $fbneo_options['fbneo_samplerate'];

          break;

        case 'fba2012cps1':
          $fba2012cps1_options = $this->getSetting('fba2012cps1_options');

          foreach ($fba2012cps1_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'fba2012cps2':
          $fba2012cps2_options = $this->getSetting('fba2012cps2_options');

          foreach ($fba2012cps2_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'mame2003':
          $mame2003_options = $this->getSetting('mame2003_options');

          foreach ($mame2003_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
