<?php

namespace Drupal\ejs_snes\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS SNES core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_super_nintendo_entertainment_system",
 *   label = @Translation("EmulatorJS SNES/Super Famicom"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSSNESFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'snes9x';

    $settings['snes9x_options'] = [
      'snes9x_justifier1_color' => "Blue",
      'snes9x_justifier1_crosshair' => "4",
      'snes9x_justifier2_color' => "Pink",
      'snes9x_justifier2_crosshair' => "4",
      'snes9x_overclock_superfx' => "100%",
      'snes9x_rifle_color' => "White",
      'snes9x_rifle_crosshair' => "2",
      'snes9x_superscope_color' => "White",
      'snes9x_superscope_crosshair' => "2",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'snes9x' => "snes9x",
    ];

    // Snes9x options.
    $snes9x_options = $this->getSetting('snes9x_options');
    $form['snes9x_options'] = [
      '#type' => 'details',
      '#title' => 'snes9x Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'snes9x'],
        ],
      ],
    ];

    $jcolors = array(
      '25% Grey' => "25% Grey",
      '25% Grey (blend)' => "25% Grey (blend)",
      '50% Grey' => "50% Grey",
      '50% Grey (blend)' => "50% Grey (blend)",
      '75% Grey' => "75% Grey",
      '75% Grey (blend)' => "75% Grey (blend)",
      'Black' => "Black",
      'Black (blend)' => "Black (blend)",
      'Blue' => "Blue",
      'Blue (blend)' => "Blue (blend)",
      'Cyan' => "Cyan",
      'Cyan (blend)' => "Cyan (blend)",
      'Green' => "Green",
      'Green (blend)' => "Green (blend)",
      'Orange' => "Orange",
      'Orange (blend)' => "Orange (blend)",
      'Pink' => "Pink",
      'Pink (blend)' => "Pink (blend)",
      'Purple' => "Purple",
      'Purple (blend)' => "Purple (blend)",
      'Red' => "Red",
      'Red (blend)' => "Red (blend)",
      'Sky' => "Sky",
      'Sky (blend)' => "Sky (blend)",
      'Violet' => "Violet",
      'Violet (blend)' => "Violet (blend)",
      'White' => "White",
      'White (blend)' => "White (blend)",
      'Yellow' => "Yellow",
      'Yellow (blend)' => "Yellow (blend)",
    );
    $zerotosixteen = [];
    for ($i = 0; $i <= 16; $i++) {
      $zerotosixteen[strval($i)] = strval($i);
    }

    $form['snes9x_options']['snes9x_justifier1_color'] = [
      '#type' => 'select',
      '#title' => 'snes9x_justifier1_color',
      '#options' => $jcolors,
      '#default_value' => $snes9x_options['snes9x_justifier1_color'],
    ];

    $form['snes9x_options']['snes9x_justifier1_crosshair`'] = [
      '#type' => 'select',
      '#title' => 'snes9x_justifier1_crosshair',
      '#options' => $zerotosixteen,
      '#default_value' => $snes9x_options['snes9x_justifier1_crosshair'],
    ];

    $form['snes9x_options']['snes9x_justifier2_color'] = [
      '#type' => 'select',
      '#title' => 'snes9x_justifier2_color',
      '#options' => $jcolors,
      '#default_value' => $snes9x_options['snes9x_justifier2_color'],
    ];

    $form['snes9x_options']['snes9x_justifier2_crosshair`'] = [
      '#type' => 'select',
      '#title' => 'snes9x_justifier2_crosshair',
      '#options' => $zerotosixteen,
      '#default_value' => $snes9x_options['snes9x_justifier2_crosshair'],
    ];

    $form['snes9x_options']['snes9x_overclock_superfx'] = [
      '#type' => 'select',
      '#title' => 'snes9x_overclock_superfx',
      '#options' => [],
      '#default_value' => $snes9x_options['snes9x_overclock_superfx'],
    ];
    for ($i = 50; $i <= 500; $i += 10) {
      $form['snes9x_options']['snes9x_overclock_superfx']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['snes9x_options']['snes9x_rifle_color'] = [
      '#type' => 'select',
      '#title' => 'snes9x_rifle_color',
      '#options' => $jcolors,
      '#default_value' => $snes9x_options['snes9x_rifle_color'],
    ];

    $form['snes9x_options']['snes9x_rifle_crosshair`'] = [
      '#type' => 'select',
      '#title' => 'snes9x_rifle_crosshair',
      '#options' => $zerotosixteen,
      '#default_value' => $snes9x_options['snes9x_rifle_crosshair'],
    ];

    $form['snes9x_options']['snes9x_superscope_color'] = [
      '#type' => 'select',
      '#title' => 'snes9x_superscope_color',
      '#options' => $jcolors,
      '#default_value' => $snes9x_options['snes9x_superscope_color'],
    ];

    $form['snes9x_options']['snes9x_superscope_crosshair`'] = [
      '#type' => 'select',
      '#title' => 'snes9x_superscope_crosshair',
      '#options' => $zerotosixteen,
      '#default_value' => $snes9x_options['snes9x_superscope_crosshair'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-snes-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-snes ejs-snes-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_snes/snes'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'snes9x':
          $snes9x_options = $this->getSetting('snes9x4_options');

          foreach ($snes9x_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
