<?php

namespace Drupal\ejs_atari\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Atari core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_atari",
 *   label = @Translation("EmulatorJS Atari"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSAtariFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'stella2014';

    $settings['stella2014_options'] = [
      'stella2014_stelladaptor_analog_center' => "0",
      'stella2014_stelladaptor_analog_sensitivity' => "20",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'Atari 2600' => [
        'stella2014' => "stella2014",
      ],
      'Atari 5200' => [
        'a5200' => "a5200",
      ],
      'Atari 7800' => [
        'prosystem' => "prosystem",
      ],
      'Atari Jaguar' => [
        'virtualjaguar' => "virtualjaguar",
      ],
      'Atari Lynx' => [
        'handy' => "handy",
      ],
    ];

    // Stella2014 options.
    $stella2014_options = $this->getSetting('stella2014_options');
    $form['stella2014_options'] = [
      '#type' => 'details',
      '#title' => 'stella2014 Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'stella2014'],
        ],
      ],
    ];

    $form['stella2014_options']['stella2014_stelladaptor_analog_center'] = [
      '#type' => 'select',
      '#title' => 'stella2014_stelladaptor_analog_center',
      '#options' => [],
      '#default_value' => $stella2014_options['stella2014_stelladaptor_analog_center'],
    ];
    for ($i = 0; $i <= 30; $i++) {
      $form['stella2014_options']['stella2014_stelladaptor_analog_center']['#options'][strval($i)] = strval($i);
    }
    for ($i = -1; $i >= -10; $i--) {
      $form['stella2014_options']['stella2014_stelladaptor_analog_center']['#options'][strval($i)] = strval($i);
    }

    $form['stella2014_options']['stella2014_stelladaptor_analog_sensitivity'] = [
      '#type' => 'select',
      '#title' => 'stella2014_stelladaptor_analog_sensitivity',
      '#options' => [],
      '#default_value' => $stella2014_options['stella2014_stelladaptor_analog_sensitivity'],
    ];
    for ($i = 0; $i <= 30; $i++) {
      $form['stella2014_options']['stella2014_stelladaptor_analog_sensitivity']['#options'][strval($i)] = strval($i);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-atari-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-atari ejs-atari-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          // 'library' => ['ejs_atari/emulatorjs'],
          'library' => ['emulatorjs/emulatorjs', 'ejs_atari/atari'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'stella2014':
          $stella2014_options = $this->getSetting('stella2014_options');

          foreach ($stella2014_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
