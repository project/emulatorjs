((Drupal, settings) => {

  Drupal.behaviors.emulatorjspsx = {
      
    attach(context, settings) {

      window.EJS_pathtodata = settings.ejs_psx.EJS_pathtodata;
      window.EJS_player = settings.ejs_psx.EJS_player;
      window.EJS_gameUrl = settings.ejs_psx.EJS_gameUrl;
      window.EJS_language = settings.ejs_psx.EJS_language;
      window.EJS_volume = settings.ejs_psx.EJS_volume;
      window.EJS_fullscreenOnLoaded = settings.ejs_psx.EJS_fullscreenOnLoaded ? true : false;
      window.EJS_startOnLoaded = settings.ejs_psx.EJS_startOnLoaded ? true : false;
      window.EJS_core = settings.ejs_psx.EJS_core;

      window.EJS_defaultOptions = settings.ejs_psx.EJS_defaultOptions;

      window.EJS_color = settings.ejs_psx.EJS_color;
      window.EJS_alignStartButton = settings.ejs_psx.EJS_alignStartButton;
      window.EJS_backgroundImage = settings.ejs_psx.EJS_backgroundImage;
      window.EJS_backgroundBlur = settings.ejs_psx.EJS_backgroundBlur ? true : false;
      window.EJS_backgroundColor = settings.ejs_psx.EJS_backgroundColor;
      window.EJS_AdUrl = settings.ejs_psx.EJS_AdUrl;
      window.EJS_AdTimer = settings.ejs_psx.EJS_AdTimer;
      window.EJS_AdMode = settings.ejs_psx.EJS_AdMode;
      window.EJS_AdSize = [settings.ejs_psx.EJS_AdSizeWidth, settings.ejs_psx.EJS_AdSizeHeight];

      window.EJS_CacheLimit = settings.ejs_psx.EJS_CacheLimit;
      window.EJS_Buttons = {
        'restart': settings.ejs_psx.EJS_Button_restart ? true : false,
        'playPause': settings.ejs_psx.EJS_Button_playPause ? true : false,
        'saveState': settings.ejs_psx.EJS_Button_saveState ? true : false,
        'loadState': settings.ejs_psx.EJS_Button_loadState ? true : false,
        'gamepad': settings.ejs_psx.EJS_Button_gamepad ? true : false,
        'cheat': settings.ejs_psx.EJS_Button_cheat ? true : false,
        'cacheManager': settings.ejs_psx.EJS_Button_cacheManager ? true : false,
        'saveSavFiles': settings.ejs_psx.EJS_Button_saveSavFiles ? true : false,
        'loadSavFiles': settings.ejs_psx.EJS_Button_loadSavFiles ? true : false,
        'mute': settings.ejs_psx.EJS_Button_mute ? true : false,
        'volume': settings.ejs_psx.EJS_Button_volume ? true : false,
        'settings': settings.ejs_psx.EJS_Button_settings ? true : false,
        'screenshot': settings.ejs_psx.EJS_Button_screenshot ? true : false,
        'screenRecord': settings.ejs_psx.EJS_Button_screenRecord ? true : false,
        'quickSave': settings.ejs_psx.EJS_Button_quickSave ? true : false,
        'quickLoad': settings.ejs_psx.EJS_Button_quickLoad ? true : false,
        'fullscreen': settings.ejs_psx.EJS_Button_fullscreen ? true : false,
      },

      window.EJS_DEBUG_XX = settings.ejs_psx.EJS_DEBUG_XX ? true : false;
      window.EJS_noAutoFocus = settings.ejs_psx.EJS_noAutoFocus ? true : false;
      window.EJS_settingsLanguage = settings.ejs_psx.EJS_settingsLanguage ? true : false;
      window.EJS_softLoad = settings.ejs_psx.EJS_softLoad ? true : false;
      window.EJS_startButtonName = settings.ejs_psx.EJS_startButtonName;

    }

  };

})(Drupal, drupalSettings);
