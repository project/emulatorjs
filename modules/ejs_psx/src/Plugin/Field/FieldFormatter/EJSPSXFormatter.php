<?php

namespace Drupal\ejs_psx\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Sony PlayStation core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_sony_playstation",
 *   label = @Translation("EmulatorJS Sony PlayStation"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSPSXFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'pcsx_rearmed';

    $settings['pcsx_options'] = [
      'pcsx_rearmed_frameskip_interval' => "3",
      'pcsx_rearmed_frameskip_threshold' => "33",
      'pcsx_rearmed_gunconadjustratiox' => "1.00",
      'pcsx_rearmed_gunconadjustratioy' => "1.00",
      'pcsx_rearmed_gunconadjustx' => "0",
      'pcsx_rearmed_gunconadjusty' => "0",
      'pcsx_rearmed_input_sensitivity' => "1.00",
      'pcsx_rearmed_konamigunadjustx' => "0",
      'pcsx_rearmed_konamigunadjusty' => "0",
      'pcsx_rearmed_psxclock' => "57",
      'pcsx_rearmed_screen_centering_x' => "0",
      'pcsx_rearmed_screen_centering_y' => "0",
    ];

    $settings['beetle_options'] = [
      'beetle_psx_hw_initial_scanline_pal' => "0",
      'beetle_psx_hw_initial_scanline' => "0",
      'beetle_psx_hw_memcard_left_index' => "0",
      'beetle_psx_hw_negcon_deadzone' => "0%",
      'beetle_psx_hw_widescreen_hack_aspect_ratio' => "16:9",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'mednafen_psx_hw' => "mednafen_psx_hw",
      'pcsx_rearmed' => "pcsx_rearmed",
    ];

    // Mednafen_psx_hw (beetle_psx_hw_*) options.
    $beetle_options = $this->getSetting('beetle_options');
    $form['beetle_options'] = [
      '#type' => 'details',
      '#title' => 'mednafen_psx_hw Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'mednafen_psx_hw'],
        ],
      ],
    ];

    $form['beetle_options']['beetle_psx_hw_initial_scanline'] = [
      '#type' => 'select',
      '#title' => 'beetle_psx_hw_initial_scanline',
      '#options' => [],
      '#default_value' => $beetle_options['beetle_psx_hw_initial_scanline'],
    ];
    for ($i = 0; $i <= 40; $i++) {
      $form['beetle_options']['beetle_psx_hw_initial_scanline']['#options'][strval($i)] = strval($i);
    }

    $form['beetle_options']['beetle_psx_hw_initial_scanline_pal'] = [
      '#type' => 'select',
      '#title' => 'beetle_psx_hw_initial_scanline_pal',
      '#options' => [],
      '#default_value' => $beetle_options['beetle_psx_hw_initial_scanline_pal'],
    ];
    for ($i = 0; $i <= 40; $i++) {
      $form['beetle_options']['beetle_psx_hw_initial_scanline_pal']['#options'][strval($i)] = strval($i);
    }

    $form['beetle_options']['beetle_psx_hw_memcard_left_index'] = [
      '#type' => 'select',
      '#title' => 'beetle_psx_hw_memcard_left_index',
      '#options' => [],
      '#default_value' => $beetle_options['beetle_psx_hw_memcard_left_index'],
    ];
    for ($i = 0; $i <= 63; $i++) {
      $form['beetle_options']['beetle_psx_hw_memcard_left_index']['#options'][strval($i)] = strval($i);
    }

    $form['beetle_options']['beetle_psx_hw_negcon_deadzone'] = [
      '#type' => 'select',
      '#title' => 'beetle_psx_hw_negcon_deadzone',
      '#options' => [],
      '#default_value' => $beetle_options['beetle_psx_hw_negcon_deadzone'],
    ];
    for ($i = 0; $i <= 30; $i += 5) {
      $form['beetle_options']['beetle_psx_hw_negcon_deadzone']['#options'][strval($i) . "%"] = strval($i) . "%";
    }

    $form['beetle_options']['beetle_psx_hw_widescreen_hack_aspect_ratio'] = [
      '#type' => 'select',
      '#title' => 'beetle_psx_hw_widescreen_hack_aspect_ratio',
      '#options' => [
        '16:9' => "16:9",
        '16:10' => "16:10",
        '18:9' => "18:9",
        '19:9' => "19:9",
        '20:9' => "20:9",
        '21:9' => "21:9",
        '32:9' => "32:9",
      ],
      '#default_value' => $beetle_options['beetle_psx_hw_widescreen_hack_aspect_ratio'],
    ];

    // Pcsx_rearmed options.
    $pcsx_options = $this->getSetting('pcsx_options');
    $form['pcsx_options'] = [
      '#type' => 'details',
      '#title' => 'pcsx_rearmed Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'pcsx_rearmed'],
        ],
      ],
    ];

    $form['pcsx_options']['pcsx_rearmed_frameskip_interval'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_frameskip_interval',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_frameskip_interval'],
    ];
    for ($i = 0; $i <= 10; $i++) {
      $form['pcsx_options']['pcsx_rearmed_frameskip_interval']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_frameskip_threshold'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_frameskip_threshold',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_frameskip_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['pcsx_options']['pcsx_rearmed_frameskip_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_gunconadjustratiox'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_gunconadjustratiox',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_gunconadjustratiox'],
    ];
    for ($i = 0.75; $i <= 1.25; $i += 0.01) {
      $form['pcsx_options']['pcsx_rearmed_gunconadjustratiox']['#options'][strval(number_format($i, 2, '.'))] = strval(number_format($i, 2, '.'));
    }

    $form['pcsx_options']['pcsx_rearmed_gunconadjustratioy'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_gunconadjustratioy',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_gunconadjustratioy'],
    ];
    for ($i = 0.75; $i <= 1.25; $i += 0.01) {
      $form['pcsx_options']['pcsx_rearmed_gunconadjustratioy']['#options'][strval(number_format($i, 2, '.'))] = strval(number_format($i, 2, '.'));
    }

    $form['pcsx_options']['pcsx_rearmed_gunconadjustx'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_gunconadjustx',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_gunconadjustx'],
    ];
    for ($i = -40; $i <= 40; $i++) {
      $form['pcsx_options']['pcsx_rearmed_gunconadjustx']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_gunconadjusty'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_gunconadjusty',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_gunconadjusty'],
    ];
    for ($i = -40; $i <= 40; $i++) {
      $form['pcsx_options']['pcsx_rearmed_gunconadjusty']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_input_sensitivity'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_input_sensitivity',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_input_sensitivity'],
    ];
    for ($i = 0.05; $i <= 2; $i += 0.05) {
      $form['pcsx_options']['pcsx_rearmed_input_sensitivity']['#options'][strval(number_format($i, 2, '.'))] = strval(number_format($i, 2, '.'));
    }

    $form['pcsx_options']['pcsx_rearmed_konamigunadjustx'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_konamigunadjustx',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_konamigunadjustx'],
    ];
    for ($i = -40; $i <= 40; $i++) {
      $form['pcsx_options']['pcsx_rearmed_konamigunadjustx']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_konamigunadjusty'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_konamigunadjusty',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_konamigunadjusty'],
    ];
    for ($i = -40; $i <= 40; $i++) {
      $form['pcsx_options']['pcsx_rearmed_konamigunadjusty']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_psxclock'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_psxclock',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_psxclock'],
    ];
    for ($i = 30; $i <= 100; $i++) {
      $form['pcsx_options']['pcsx_rearmed_psxclock']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_screen_centering_x'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_screen_centering_x',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_screen_centering_x'],
    ];
    for ($i = -16; $i <= 16; $i += 2) {
      $form['pcsx_options']['pcsx_rearmed_screen_centering_x']['#options'][strval($i)] = strval($i);
    }

    $form['pcsx_options']['pcsx_rearmed_screen_centering_y'] = [
      '#type' => 'select',
      '#title' => 'pcsx_rearmed_screen_centering_y',
      '#options' => [],
      '#default_value' => $pcsx_options['pcsx_rearmed_screen_centering_y'],
    ];
    for ($i = -16; $i <= 16; $i += 2) {
      $form['pcsx_options']['pcsx_rearmed_screen_centering_y']['#options'][strval($i)] = strval($i);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-psx-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-psx ejs-psx-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_psx/psx'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'pcsx_rearmed':
          $pcsx_options = $this->getSetting('pcsx_options');

          foreach ($pcsx_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'mednafen_psx_hw':
          $beetle_options = $this->getSetting('beetle_options');

          foreach ($beetle_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }

    }

    return $elements;
  }

}
