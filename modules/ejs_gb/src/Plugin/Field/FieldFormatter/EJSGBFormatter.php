<?php

namespace Drupal\ejs_gb\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\emulatorjs\Plugin\Field\FieldFormatter\EJSFormatterBase;

/**
 * Plugin implementation of EmulatorJS Nintendo Gameboy core formatter.
 *
 * @FieldFormatter(
 *   id = "file_emulatorjs_nintendo_game_boy",
 *   label = @Translation("EmulatorJS Nintendo Game Boy/Color/Super/Advance"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EJSGBFormatter extends EJSFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['core_options']['EJS_core'] = 'gambatte';

    $settings['gambatte_options'] = [
      'gambatte_dark_filter_level' => "0",
      'gambatte_gb_hwmode' => "Auto",
      'gambatte_gb_internal_palette' => "GB - DMG",
      'gambatte_gb_palette_pixelshift_1' => "PixelShift 01 - Arctic Green",
      'gambatte_gb_palette_twb64_1' => "TWB64 001 - Aqours Blue",
      'gambatte_gb_palette_twb64_2' => "TWB64 101 - 765PRO Pink",
      'gambatte_gb_palette_twb64_3' => "TWB64 201 - DMG-GOLD",
      'gambatte_rumble_level' => "4",
      'gambatte_turbo_period' => "10",
    ];

    $settings['mgba_options'] = [
      'mgba_color_correction' => "OFF",
      'mgba_force_gbp' => "OFF",
      'mgba_frameskip_interval' => "0",
      'mgba_frameskip_threshold' => "33",
      'mgba_gb_colors' => "Grayscale",
      'mgba_gb_model' => "Autodetect",
      'mgba_idle_optimization' => "Remove Known",
      'mgba_sgb_borders' => "ON",
      'mgba_skip_bios' => "OFF",
      'mgba_solar_sensor_level' => "0",
      'mgba_use_bios' => "ON",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['core_options']['EJS_core']['#options'] = [
      'gambatte' => "gambatte",
      'mgba' => "mgba",
    ];

    // Gambatte options.
    $gambatte_options = $this->getSetting('gambatte_options');
    $form['gambatte_options'] = [
      '#type' => 'details',
      '#title' => 'gambatte Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'gambatte'],
        ],
      ],
    ];

    $form['gambatte_options']['gambatte_dark_filter_level'] = [
      '#type' => 'select',
      '#title' => 'gambatte_dark_filter_level',
      '#options' => [],
      '#default_value' => $gambatte_options['gambatte_dark_filter_level'],
    ];
    for ($i = 0; $i <= 50; $i += 5) {
      $form['gambatte_options']['gambatte_dark_filter_level']['#options'][strval($i)] = strval($i);
    }

    $form['gambatte_options']['gambatte_gb_hwmode'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_hwmode',
      '#options' => [
        'Auto' => "Auto",
        'GB' => "GB",
        'GBC' => "GBC",
        'GBA' => "GBA",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_hwmode'],
    ];

    $form['gambatte_options']['gambatte_gb_internal_palette'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_internal_palette',
      '#options' => [
        'GB - DMG' => "GB - DMG",
        'GB - Light' => "GB - Light",
        'GB - Pocket' => "GB - Pocket",
        'GBC - Blue' => "GBC - Blue",
        'GBC - Brown' => "GBC - Brown",
        'GBC - Dark Blue' => "GBC - Dark Blue",
        'GBC - Dark Brown' => "GBC - Dark Brown",
        'GBC - Dark Green' => "GBC - Dark Green",
        'GBC - Grayscale' => "GBC - Grayscale",
        'GBC - Green' => "GBC - Green",
        'GBC - Inverted' => "GBC - Inverted",
        'GBC - Orange' => "GBC - Orange",
        'GBC - Pastel Mix' => "GBC - Pastel Mix",
        'GBC - Red' => "GBC - Red",
        'GBC - Yellow' => "GBC - Yellow",
        'PixelShift - Pack 1' => "PixelShift - Pack 1",
        'SGB - 1A' => "SGB - 1A",
        'SGB - 1B' => "SGB - 1B",
        'SGB - 1C' => "SGB - 1C",
        'SGB - 1D' => "SGB - 1D",
        'SGB - 1E' => "SGB - 1E",
        'SGB - 1F' => "SGB - 1F",
        'SGB - 1G' => "SGB - 1G",
        'SGB - 1H' => "SGB - 1H",
        'SGB - 2A' => "SGB - 2A",
        'SGB - 2B' => "SGB - 2B",
        'SGB - 2C' => "SGB - 2C",
        'SGB - 2D' => "SGB - 2D",
        'SGB - 2E' => "SGB - 2E",
        'SGB - 2F' => "SGB - 2F",
        'SGB - 2G' => "SGB - 2G",
        'SGB - 2H' => "SGB - 2H",
        'SGB - 3A' => "SGB - 3A",
        'SGB - 3B' => "SGB - 3B",
        'SGB - 3C' => "SGB - 3C",
        'SGB - 3D' => "SGB - 3D",
        'SGB - 3E' => "SGB - 3E",
        'SGB - 3F' => "SGB - 3F",
        'SGB - 3G' => "SGB - 3G",
        'SGB - 3H' => "SGB - 3H",
        'SGB - 4A' => "SGB - 4A",
        'SGB - 4B' => "SGB - 4B",
        'SGB - 4C' => "SGB - 4C",
        'SGB - 4D' => "SGB - 4D",
        'SGB - 4E' => "SGB - 4E",
        'SGB - 4F' => "SGB - 4F",
        'SGB - 4G' => "SGB - 4G",
        'SGB - 4H' => "SGB - 4H",
        'Special 1' => "Special 1",
        'Special 2' => "Special 2",
        'Special 3' => "Special 3",
        'Special 4 (TI-83 Legacy)' => "Special 4 (TI-83 Legacy)",
        'TWB64 - Pack 1' => "TWB64 - Pack 1",
        'TWB64 - Pack 2' => "TWB64 - Pack 2",
        'TWB64 - Pack 3' => "TWB64 - Pack 3",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_internal_palette'],
    ];

    $form['gambatte_options']['gambatte_gb_palette_pixelshift_1'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_palette_pixelshift_1',
      '#options' => [
        'PixelShift 01 - Arctic Green' => "PixelShift 01 - Arctic Green",
        'PixelShift 02 - Arduboy' => "PixelShift 02 - Arduboy",
        'PixelShift 03 - BGB 0.3 Emulator' => "PixelShift 03 - BGB 0.3 Emulator",
        'PixelShift 04 - Camouflage' => "PixelShift 04 - Camouflage",
        'PixelShift 05 - Chocolate Bar' => "PixelShift 05 - Chocolate Bar",
        'PixelShift 06 - CMYK' => "PixelShift 06 - CMYK",
        'PixelShift 07 - Cotton Candy' => "PixelShift 07 - Cotton Candy",
        'PixelShift 08 - Easy Greens' => "PixelShift 08 - Easy Greens",
        'PixelShift 09 - Gamate' => "PixelShift 09 - Gamate",
        'PixelShift 10 - Game Boy Light' => "PixelShift 10 - Game Boy Light",
        'PixelShift 11 - Game Boy Pocket' => "PixelShift 11 - Game Boy Pocket",
        'PixelShift 12 - Game Boy Pocket Alt' => "PixelShift 12 - Game Boy Pocket Alt",
        'PixelShift 13 - Game Pocket Computer' => "PixelShift 13 - Game Pocket Computer",
        'PixelShift 14 - Game & Watch Ball' => "PixelShift 14 - Game & Watch Ball",
        'PixelShift 15 - GB Backlight Blue' => "PixelShift 15 - GB Backlight Blue",
        'PixelShift 16 - GB Backlight Faded' => "PixelShift 16 - GB Backlight Faded",
        'PixelShift 17 - GB Backlight Orange' => "PixelShift 17 - GB Backlight Orange",
        'PixelShift 18 - GB Backlight White ' => "PixelShift 18 - GB Backlight White ",
        'PixelShift 19 - GB Backlight Yellow Dark' => "PixelShift 19 - GB Backlight Yellow Dark",
        'PixelShift 20 - GB Bootleg' => "PixelShift 20 - GB Bootleg",
        'PixelShift 21 - GB Hunter' => "PixelShift 21 - GB Hunter",
        'PixelShift 22 - GB Kiosk' => "PixelShift 22 - GB Kiosk",
        'PixelShift 23 - GB Kiosk 2' => "PixelShift 23 - GB Kiosk 2",
        'PixelShift 24 - GB New' => "PixelShift 24 - GB New",
        'PixelShift 25 - GB Nuked' => "PixelShift 25 - GB Nuked",
        'PixelShift 26 - GB Old' => "PixelShift 26 - GB Old",
        'PixelShift 27 - GBP Bivert' => "PixelShift 27 - GBP Bivert",
        'PixelShift 28 - GB Washed Yellow Backlight' => "PixelShift 28 - GB Washed Yellow Backlight",
        'PixelShift 29 - Ghost' => "PixelShift 29 - Ghost",
        'PixelShift 30 - Glow In The Dark' => "PixelShift 30 - Glow In The Dark",
        'PixelShift 31 - Gold Bar' => "PixelShift 31 - Gold Bar",
        'PixelShift 32 - Grapefruit' => "PixelShift 32 - Grapefruit",
        'PixelShift 33 - Gray Green Mix' => "PixelShift 33 - Gray Green Mix",
        'PixelShift 34 - Missingno' => "PixelShift 34 - Missingno",
        'PixelShift 35 - MS-Dos' => "PixelShift 35 - MS-Dos",
        'PixelShift 36 - Newspaper' => "PixelShift 36 - Newspaper",
        'PixelShift 37 - Pip-Boy' => "PixelShift 37 - Pip-Boy",
        'PixelShift 38 - Pocket Girl' => "PixelShift 38 - Pocket Girl",
        'PixelShift 39 - Silhouette' => "PixelShift 39 - Silhouette",
        'PixelShift 40 - Sunburst' => "PixelShift 40 - Sunburst",
        'PixelShift 41 - Technicolor' => "PixelShift 41 - Technicolor",
        'PixelShift 42 - Tron' => "PixelShift 42 - Tron",
        'PixelShift 43 - Vaporwave' => "PixelShift 43 - Vaporwave",
        'PixelShift 44 - Virtual Boy' => "PixelShift 44 - Virtual Boy",
        'PixelShift 45 - Wish' => "PixelShift 45 - Wish",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_palette_pixelshift_1'],
    ];

    $form['gambatte_options']['gambatte_gb_palette_twb64_1'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_palette_twb64_1',
      '#options' => [
        'TWB64 001 - Aqours Blue' => "TWB64 001 - Aqours Blue",
        'TWB64 002 - Anime Expo Ver.' => "TWB64 002 - Anime Expo Ver.",
        'TWB64 003 - SpongeBob Yellow' => "TWB64 003 - SpongeBob Yellow",
        'TWB64 004 - Patrick Star Pink' => "TWB64 004 - Patrick Star Pink",
        'TWB64 005 - Neon Red' => "TWB64 005 - Neon Red",
        'TWB64 006 - Neon Blue' => "TWB64 006 - Neon Blue",
        'TWB64 007 - Neon Yellow' => "TWB64 007 - Neon Yellow",
        'TWB64 008 - Neon Green' => "TWB64 008 - Neon Green",
        'TWB64 009 - Neon Pink' => "TWB64 009 - Neon Pink",
        'TWB64 010 - Mario Red' => "TWB64 010 - Mario Red",
        'TWB64 011 - Nick Orange' => "TWB64 011 - Nick Orange",
        'TWB64 012 - Virtual Vision' => "TWB64 012 - Virtual Vision",
        'TWB64 013 - Golden Wild' => "TWB64 013 - Golden Wild",
        'TWB64 014 - DMG-099' => "TWB64 014 - DMG-099",
        'TWB64 015 - Classic Blurple' => "TWB64 015 - Classic Blurple",
        'TWB64 016 - 765 Production Ver.' => "TWB64 016 - 765 Production Ver.",
        'TWB64 017 - Superball Ivory' => "TWB64 017 - Superball Ivory",
        'TWB64 018 - Crunchyroll Orange' => "TWB64 018 - Crunchyroll Orange",
        'TWB64 019 - Muse Pink' => "TWB64 019 - Muse Pink",
        'TWB64 020 - School Idol Blue' => "TWB64 020 - School Idol Blue",
        'TWB64 021 - Gamate Ver.' => "TWB64 021 - Gamate Ver.",
        'TWB64 022 - Greenscale Ver.' => "TWB64 022 - Greenscale Ver.",
        'TWB64 023 - Odyssey Gold' => "TWB64 023 - Odyssey Gold",
        'TWB64 024 - Super Saiyan God' => "TWB64 024 - Super Saiyan God",
        'TWB64 025 - Super Saiyan Blue' => "TWB64 025 - Super Saiyan Blue",
        'TWB64 026 - ANIMAX BLUE' => "TWB64 026 - ANIMAX BLUE",
        'TWB64 027 - BMO Ver.' => "TWB64 027 - BMO Ver.",
        'TWB64 028 - Game.com Ver.' => "TWB64 028 - Game.com Ver.",
        'TWB64 029 - Sanrio Pink' => "TWB64 029 - Sanrio Pink",
        'TWB64 030 - Timmy Turner Pink' => "TWB64 030 - Timmy Turner Pink",
        'TWB64 031 - Fairly OddPalette' => "TWB64 031 - Fairly OddPalette",
        'TWB64 032 - Danny Phantom Silver' => "TWB64 032 - Danny Phantom Silver",
        'TWB64 033 - Link\'s Awakening DX Ver.' => "TWB64 033 - Link's Awakening DX Ver.",
        'TWB64 034 - Travel Wood' => "TWB64 034 - Travel Wood",
        'TWB64 035 - Pokemon Ver.' => "TWB64 035 - Pokemon Ver.",
        'TWB64 036 - Game Grump Orange' => "TWB64 036 - Game Grump Orange",
        'TWB64 037 - Scooby-Doo Mystery Ver.' => "TWB64 037 - Scooby-Doo Mystery Ver.",
        'TWB64 038 - Pokemon mini Ver.' => "TWB64 038 - Pokemon mini Ver.",
        'TWB64 039 - Supervision Ver.' => "TWB64 039 - Supervision Ver.",
        'TWB64 040 - DMG Ver.' => "TWB64 040 - DMG Ver.",
        'TWB64 041 - Pocket Ver.' => "TWB64 041 - Pocket Ver.",
        'TWB64 042 - Light Ver.' => "TWB64 042 - Light Ver.",
        'TWB64 043 - All Might Hero Palette' => "TWB64 043 - All Might Hero Palette",
        'TWB64 044 - U.A. High School Uniform' => "TWB64 044 - U.A. High School Uniform",
        'TWB64 045 - Pikachu Yellow' => "TWB64 045 - Pikachu Yellow",
        'TWB64 046 - Eevee Brown' => "TWB64 046 - Eevee Brown",
        'TWB64 047 - Microvision Ver.' => "TWB64 047 - Microvision Ver.",
        'TWB64 048 - TI-83 Ver.' => "TWB64 048 - TI-83 Ver.",
        'TWB64 049 - Aegis Cherry' => "TWB64 049 - Aegis Cherry",
        'TWB64 050 - Labo Fawn' => "TWB64 050 - Labo Fawn",
        'TWB64 051 - MILLION LIVE GOLD!' => "TWB64 051 - MILLION LIVE GOLD!",
        'TWB64 052 - Squidward Sea Foam Green' => "TWB64 052 - Squidward Sea Foam Green",
        'TWB64 053 - VMU Ver.' => "TWB64 053 - VMU Ver.",
        'TWB64 054 - Game Master Ver.' => "TWB64 054 - Game Master Ver.",
        'TWB64 055 - Android Green' => "TWB64 055 - Android Green",
        'TWB64 056 - Amazon Vision' => "TWB64 056 - Amazon Vision",
        'TWB64 057 - Google Red' => "TWB64 057 - Google Red",
        'TWB64 058 - Google Blue' => "TWB64 058 - Google Blue",
        'TWB64 059 - Google Yellow' => "TWB64 059 - Google Yellow",
        'TWB64 060 - Google Green' => "TWB64 060 - Google Green",
        'TWB64 061 - WonderSwan Ver.' => "TWB64 061 - WonderSwan Ver.",
        'TWB64 062 - Neo Geo Pocket Ver.' => "TWB64 062 - Neo Geo Pocket Ver.",
        'TWB64 063 - Dew Green' => "TWB64 063 - Dew Green",
        'TWB64 064 - Coca-Cola Vision' => "TWB64 064 - Coca-Cola Vision",
        'TWB64 065 - GameKing Ver.' => "TWB64 065 - GameKing Ver.",
        'TWB64 066 - Do The Dew Ver.' => "TWB64 066 - Do The Dew Ver.",
        'TWB64 067 - Digivice Ver.' => "TWB64 067 - Digivice Ver.",
        'TWB64 068 - Bikini Bottom Ver.' => "TWB64 068 - Bikini Bottom Ver.",
        'TWB64 069 - Blossom Pink' => "TWB64 069 - Blossom Pink",
        'TWB64 070 - Bubbles Blue' => "TWB64 070 - Bubbles Blue",
        'TWB64 071 - Buttercup Green' => "TWB64 071 - Buttercup Green",
        'TWB64 072 - NASCAR Ver.' => "TWB64 072 - NASCAR Ver.",
        'TWB64 073 - Lemon-Lime Green' => "TWB64 073 - Lemon-Lime Green",
        'TWB64 074 - Mega Man V Ver.' => "TWB64 074 - Mega Man V Ver.",
        'TWB64 075 - Tamagotchi Ver.' => "TWB64 075 - Tamagotchi Ver.",
        'TWB64 076 - Phantom Red' => "TWB64 076 - Phantom Red",
        'TWB64 077 - Halloween Ver.' => "TWB64 077 - Halloween Ver.",
        'TWB64 078 - Christmas Ver.' => "TWB64 078 - Christmas Ver.",
        'TWB64 079 - Cardcaptor Pink' => "TWB64 079 - Cardcaptor Pink",
        'TWB64 080 - Pretty Guardian Gold' => "TWB64 080 - Pretty Guardian Gold",
        'TWB64 081 - Camouflage Ver.' => "TWB64 081 - Camouflage Ver.",
        'TWB64 082 - Legendary Super Saiyan' => "TWB64 082 - Legendary Super Saiyan",
        'TWB64 083 - Super Saiyan Rose' => "TWB64 083 - Super Saiyan Rose",
        'TWB64 084 - Super Saiyan' => "TWB64 084 - Super Saiyan",
        'TWB64 085 - Perfected Ultra Instinct' => "TWB64 085 - Perfected Ultra Instinct",
        'TWB64 086 - Saint Snow Red' => "TWB64 086 - Saint Snow Red",
        'TWB64 087 - Yellow Banana' => "TWB64 087 - Yellow Banana",
        'TWB64 088 - Green Banana' => "TWB64 088 - Green Banana",
        'TWB64 089 - Super Saiyan 3' => "TWB64 089 - Super Saiyan 3",
        'TWB64 090 - Super Saiyan Blue Evolved' => "TWB64 090 - Super Saiyan Blue Evolved",
        'TWB64 091 - Pocket Tales Ver.' => "TWB64 091 - Pocket Tales Ver.",
        'TWB64 092 - Investigation Yellow' => "TWB64 092 - Investigation Yellow",
        'TWB64 093 - S.E.E.S. Blue' => "TWB64 093 - S.E.E.S. Blue",
        'TWB64 094 - Ultra Instinct Sign' => "TWB64 094 - Ultra Instinct Sign",
        'TWB64 095 - Hokage Orange' => "TWB64 095 - Hokage Orange",
        'TWB64 096 - Straw Hat Red' => "TWB64 096 - Straw Hat Red",
        'TWB64 097 - Sword Art Cyan' => "TWB64 097 - Sword Art Cyan",
        'TWB64 098 - Deku Alpha Emerald' => "TWB64 098 - Deku Alpha Emerald",
        'TWB64 099 - Blue Stripes Ver.' => "TWB64 099 - Blue Stripes Ver.",
        'TWB64 100 - Precure Marble Raspberry' => "TWB64 100 - Precure Marble Raspberry",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_palette_twb64_1'],
    ];

    $form['gambatte_options']['gambatte_gb_palette_twb64_2'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_palette_twb64_2',
      '#options' => [
        'TWB64 101 - 765PRO Pink' => "TWB64 101 - 765PRO Pink",
        'TWB64 102 - CINDERELLA Blue' => "TWB64 102 - CINDERELLA Blue",
        'TWB64 103 - MILLION Yellow!' => "TWB64 103 - MILLION Yellow!",
        'TWB64 104 - SideM Green' => "TWB64 104 - SideM Green",
        'TWB64 105 - SHINY Sky Blue' => "TWB64 105 - SHINY Sky Blue",
        'TWB64 106 - Angry Volcano Ver.' => "TWB64 106 - Angry Volcano Ver.",
        'TWB64 107 - NBA Vision' => "TWB64 107 - NBA Vision",
        'TWB64 108 - NFL Vision' => "TWB64 108 - NFL Vision",
        'TWB64 109 - MLB Vision' => "TWB64 109 - MLB Vision",
        'TWB64 110 - Anime Digivice Ver.' => "TWB64 110 - Anime Digivice Ver.",
        'TWB64 111 - Aquatic Iro' => "TWB64 111 - Aquatic Iro",
        'TWB64 112 - Tea Midori' => "TWB64 112 - Tea Midori",
        'TWB64 113 - Sakura Pink' => "TWB64 113 - Sakura Pink",
        'TWB64 114 - Wisteria Murasaki' => "TWB64 114 - Wisteria Murasaki",
        'TWB64 115 - Oni Aka' => "TWB64 115 - Oni Aka",
        'TWB64 116 - Golden Kiiro' => "TWB64 116 - Golden Kiiro",
        'TWB64 117 - Silver Shiro' => "TWB64 117 - Silver Shiro",
        'TWB64 118 - Fruity Orange' => "TWB64 118 - Fruity Orange",
        'TWB64 119 - AKB48 Pink' => "TWB64 119 - AKB48 Pink",
        'TWB64 120 - Miku Blue' => "TWB64 120 - Miku Blue",
        'TWB64 121 - Tri Digivice Ver.' => "TWB64 121 - Tri Digivice Ver.",
        'TWB64 122 - Survey Corps Uniform' => "TWB64 122 - Survey Corps Uniform",
        'TWB64 123 - Island Green' => "TWB64 123 - Island Green",
        'TWB64 124 - Nogizaka46 Purple' => "TWB64 124 - Nogizaka46 Purple",
        'TWB64 125 - Ninja Turtle Green' => "TWB64 125 - Ninja Turtle Green",
        'TWB64 126 - Slime Blue' => "TWB64 126 - Slime Blue",
        'TWB64 127 - Lime Midori' => "TWB64 127 - Lime Midori",
        'TWB64 128 - Ghostly Aoi' => "TWB64 128 - Ghostly Aoi",
        'TWB64 129 - Retro Bogeda' => "TWB64 129 - Retro Bogeda",
        'TWB64 130 - Royal Blue' => "TWB64 130 - Royal Blue",
        'TWB64 131 - Neon Purple' => "TWB64 131 - Neon Purple",
        'TWB64 132 - Neon Orange' => "TWB64 132 - Neon Orange",
        'TWB64 133 - Moonlight Vision' => "TWB64 133 - Moonlight Vision",
        'TWB64 134 - Rising Sun Red' => "TWB64 134 - Rising Sun Red",
        'TWB64 135 - Burger King Color Combo' => "TWB64 135 - Burger King Color Combo",
        'TWB64 136 - Grand Zeno Coat' => "TWB64 136 - Grand Zeno Coat",
        'TWB64 137 - Pac-Man Yellow' => "TWB64 137 - Pac-Man Yellow",
        'TWB64 138 - Irish Green' => "TWB64 138 - Irish Green",
        'TWB64 139 - Goku Gi' => "TWB64 139 - Goku Gi",
        'TWB64 140 - Dragon Ball Orange' => "TWB64 140 - Dragon Ball Orange",
        'TWB64 141 - Christmas Gold' => "TWB64 141 - Christmas Gold",
        'TWB64 142 - Pepsi Vision' => "TWB64 142 - Pepsi Vision",
        'TWB64 143 - Bubblun Green' => "TWB64 143 - Bubblun Green",
        'TWB64 144 - Bobblun Blue' => "TWB64 144 - Bobblun Blue",
        'TWB64 145 - Baja Blast Storm' => "TWB64 145 - Baja Blast Storm",
        'TWB64 146 - Olympic Gold' => "TWB64 146 - Olympic Gold",
        'TWB64 147 - LisAni Orange!' => "TWB64 147 - LisAni Orange!",
        'TWB64 148 - Liella Purple!' => "TWB64 148 - Liella Purple!",
        'TWB64 149 - Olympic Silver' => "TWB64 149 - Olympic Silver",
        'TWB64 150 - Olympic Bronze' => "TWB64 150 - Olympic Bronze",
        'TWB64 151 - ANA Flight Blue' => "TWB64 151 - ANA Flight Blue",
        'TWB64 152 - Nijigasaki Orange' => "TWB64 152 - Nijigasaki Orange",
        'TWB64 153 - holoblue' => "TWB64 153 - holoblue",
        'TWB64 154 - WWE White and Red' => "TWB64 154 - WWE White and Red",
        'TWB64 155 - Yoshi Egg Green' => "TWB64 155 - Yoshi Egg Green",
        'TWB64 156 - Pokedex Red' => "TWB64 156 - Pokedex Red",
        'TWB64 157 - FamilyMart Vision' => "TWB64 157 - FamilyMart Vision",
        'TWB64 158 - Xbox Green' => "TWB64 158 - Xbox Green",
        'TWB64 159 - Sonic Mega Blue' => "TWB64 159 - Sonic Mega Blue",
        'TWB64 160 - Sprite Green' => "TWB64 160 - Sprite Green",
        'TWB64 161 - Scarlett Green' => "TWB64 161 - Scarlett Green",
        'TWB64 162 - Glitchy Blue' => "TWB64 162 - Glitchy Blue",
        'TWB64 163 - Classic LCD' => "TWB64 163 - Classic LCD",
        'TWB64 164 - 3DS Virtual Console Ver.' => "TWB64 164 - 3DS Virtual Console Ver.",
        'TWB64 165 - PocketStation Ver.' => "TWB64 165 - PocketStation Ver.",
        'TWB64 166 - Timeless Gold and Red' => "TWB64 166 - Timeless Gold and Red",
        'TWB64 167 - Smurfy Blue' => "TWB64 167 - Smurfy Blue",
        'TWB64 168 - Swampy Ogre Green' => "TWB64 168 - Swampy Ogre Green",
        'TWB64 169 - Sailor Spinach Green' => "TWB64 169 - Sailor Spinach Green",
        'TWB64 170 - Shenron Green' => "TWB64 170 - Shenron Green",
        'TWB64 171 - Berserk Blood' => "TWB64 171 - Berserk Blood",
        'TWB64 172 - Super Star Pink' => "TWB64 172 - Super Star Pink",
        'TWB64 173 - Gamebuino Classic Ver.' => "TWB64 173 - Gamebuino Classic Ver.",
        'TWB64 174 - Barbie Pink' => "TWB64 174 - Barbie Pink",
        'TWB64 175 - YOASOBI AMARANTH' => "TWB64 175 - YOASOBI AMARANTH",
        'TWB64 176 - Nokia 3310 Ver.' => "TWB64 176 - Nokia 3310 Ver.",
        'TWB64 177 - Clover Green' => "TWB64 177 - Clover Green",
        'TWB64 178 - Goku GT Gi' => "TWB64 178 - Goku GT Gi",
        'TWB64 179 - Famicom Disk Yellow' => "TWB64 179 - Famicom Disk Yellow",
        'TWB64 180 - Team Rocket Uniform' => "TWB64 180 - Team Rocket Uniform",
        'TWB64 181 - SEIKO Timely Vision' => "TWB64 181 - SEIKO Timely Vision",
        'TWB64 182 - PASTEL109' => "TWB64 182 - PASTEL109",
        'TWB64 183 - Doraemon Tricolor' => "TWB64 183 - Doraemon Tricolor",
        'TWB64 184 - Fury Blue' => "TWB64 184 - Fury Blue",
        'TWB64 185 - GOOD SMILE VISION' => "TWB64 185 - GOOD SMILE VISION",
        'TWB64 186 - Puyo Puyo Green' => "TWB64 186 - Puyo Puyo Green",
        'TWB64 187 - Circle K Color Combo' => "TWB64 187 - Circle K Color Combo",
        'TWB64 188 - Pizza Hut Red' => "TWB64 188 - Pizza Hut Red",
        'TWB64 189 - Emerald Green' => "TWB64 189 - Emerald Green",
        'TWB64 190 - Grand Ivory' => "TWB64 190 - Grand Ivory",
        'TWB64 191 - Demon\'s Gold' => "TWB64 191 - Demon's Gold",
        'TWB64 192 - SEGA Tokyo Blue' => "TWB64 192 - SEGA Tokyo Blue",
        'TWB64 193 - Champion\'s Tunic' => "TWB64 193 - Champion's Tunic",
        'TWB64 194 - DK Barrel Brown' => "TWB64 194 - DK Barrel Brown",
        'TWB64 195 - EVA-01' => "TWB64 195 - EVA-01",
        'TWB64 196 - Wild West Vision' => "TWB64 196 - Wild West Vision",
        'TWB64 197 - Optimus Prime Palette' => "TWB64 197 - Optimus Prime Palette",
        'TWB64 198 - niconico sea green' => "TWB64 198 - niconico sea green",
        'TWB64 199 - Duracell Copper' => "TWB64 199 - Duracell Copper",
        'TWB64 200 - TOKYO SKYTREE CLOUDY BLUE' => "TWB64 200 - TOKYO SKYTREE CLOUDY BLUE",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_palette_twb64_2'],
    ];

    $form['gambatte_options']['gambatte_gb_palette_twb64_3'] = [
      '#type' => 'select',
      '#title' => 'gambatte_gb_palette_twb64_3',
      '#options' => [
        'TWB64 201 - DMG-GOLD' => "TWB64 201 - DMG-GOLD",
        'TWB64 202 - LCD Clock Green' => "TWB64 202 - LCD Clock Green",
        'TWB64 203 - Famicom Frenzy' => "TWB64 203 - Famicom Frenzy",
        'TWB64 204 - DK Arcade Blue' => "TWB64 204 - DK Arcade Blue",
        'TWB64 205 - Advanced Indigo' => "TWB64 205 - Advanced Indigo",
        'TWB64 206 - Ultra Black' => "TWB64 206 - Ultra Black",
        'TWB64 207 - Chaos Emerald Green' => "TWB64 207 - Chaos Emerald Green",
        'TWB64 208 - Blue Bomber Vision' => "TWB64 208 - Blue Bomber Vision",
        'TWB64 209 - Krispy Kreme Vision' => "TWB64 209 - Krispy Kreme Vision",
        'TWB64 210 - Steam Gray' => "TWB64 210 - Steam Gray",
        'TWB64 211 - Dream Land GB Ver.' => "TWB64 211 - Dream Land GB Ver.",
        'TWB64 212 - Pokemon Pinball Ver.' => "TWB64 212 - Pokemon Pinball Ver.",
        'TWB64 213 - Poketch Ver.' => "TWB64 213 - Poketch Ver.",
        'TWB64 214 - COLLECTION of SaGa Ver.' => "TWB64 214 - COLLECTION of SaGa Ver.",
        'TWB64 215 - Rocky-Valley Holiday' => "TWB64 215 - Rocky-Valley Holiday",
        'TWB64 216 - Giga Kiwi DMG' => "TWB64 216 - Giga Kiwi DMG",
        'TWB64 217 - DMG Pea Green' => "TWB64 217 - DMG Pea Green",
        'TWB64 218 - Timing Hero Ver.' => "TWB64 218 - Timing Hero Ver.",
        'TWB64 219 - Invincible Yellow and Blue' => "TWB64 219 - Invincible Yellow and Blue",
        'TWB64 220 - Grinchy Green' => "TWB64 220 - Grinchy Green",
        'TWB64 221 - animate vision' => "TWB64 221 - animate vision",
        'TWB64 222 - School Idol Mix' => "TWB64 222 - School Idol Mix",
        'TWB64 223 - Green Awakening' => "TWB64 223 - Green Awakening",
        'TWB64 224 - Goomba Brown' => "TWB64 224 - Goomba Brown",
        'TWB64 225 - WarioWare MicroBlue' => "TWB64 225 - WarioWare MicroBlue",
        'TWB64 226 - KonoSuba Sherbet' => "TWB64 226 - KonoSuba Sherbet",
        'TWB64 227 - Spooky Purple' => "TWB64 227 - Spooky Purple",
        'TWB64 228 - Treasure Gold' => "TWB64 228 - Treasure Gold",
        'TWB64 229 - Cherry Blossom Pink' => "TWB64 229 - Cherry Blossom Pink",
        'TWB64 230 - Golden Trophy' => "TWB64 230 - Golden Trophy",
        'TWB64 231 - Glacial Winter Blue' => "TWB64 231 - Glacial Winter Blue",
        'TWB64 232 - Leprechaun Green' => "TWB64 232 - Leprechaun Green",
        'TWB64 233 - SAITAMA SUPER BLUE' => "TWB64 233 - SAITAMA SUPER BLUE",
        'TWB64 234 - SAITAMA SUPER GREEN' => "TWB64 234 - SAITAMA SUPER GREEN",
        'TWB64 235 - Duolingo Green' => "TWB64 235 - Duolingo Green",
        'TWB64 236 - Super Mushroom Vision' => "TWB64 236 - Super Mushroom Vision",
        'TWB64 237 - Ancient Hisuian Brown' => "TWB64 237 - Ancient Hisuian Brown",
        'TWB64 238 - Sky Pop Ivory' => "TWB64 238 - Sky Pop Ivory",
        'TWB64 239 - LAWSON BLUE' => "TWB64 239 - LAWSON BLUE",
        'TWB64 240 - Anime Expo Red' => "TWB64 240 - Anime Expo Red",
        'TWB64 241 - Brilliant Diamond Blue' => "TWB64 241 - Brilliant Diamond Blue",
        'TWB64 242 - Shining Pearl Pink' => "TWB64 242 - Shining Pearl Pink",
        'TWB64 243 - Funimation Melon' => "TWB64 243 - Funimation Melon",
        'TWB64 244 - Teyvat Brown' => "TWB64 244 - Teyvat Brown",
        'TWB64 245 - Chozo Blue' => "TWB64 245 - Chozo Blue",
        'TWB64 246 - Spotify Green' => "TWB64 246 - Spotify Green",
        'TWB64 247 - Dr Pepper Red' => "TWB64 247 - Dr Pepper Red",
        'TWB64 248 - NHK Silver Gray' => "TWB64 248 - NHK Silver Gray",
        'TWB64 249 - Dunkin\' Vision' => "TWB64 249 - Dunkin' Vision",
        'TWB64 250 - Deku Gamma Palette' => "TWB64 250 - Deku Gamma Palette",
        'TWB64 251 - Universal Studios Blue' => "TWB64 251 - Universal Studios Blue",
        'TWB64 252 - Hogwarts Goldius' => "TWB64 252 - Hogwarts Goldius",
        'TWB64 253 - Kentucky Fried Red' => "TWB64 253 - Kentucky Fried Red",
        'TWB64 254 - Cheeto Orange' => "TWB64 254 - Cheeto Orange",
        'TWB64 255 - Namco Idol Pink' => "TWB64 255 - Namco Idol Pink",
        'TWB64 256 - Domino\'s Pizza Vision' => "TWB64 256 - Domino's Pizza Vision",
        'TWB64 257 - Pac-Man Vision' => "TWB64 257 - Pac-Man Vision",
        'TWB64 258 - Bill\'s PC Screen' => "TWB64 258 - Bill's PC Screen",
        'TWB64 259 - Sonic Mega Blue' => "TWB64 259 - Sonic Mega Blue",
        'TWB64 260 - Fool\'s Gold and Silver' => "TWB64 260 - Fool's Gold and Silver",
        'TWB64 261 - UTA VISION' => "TWB64 261 - UTA VISION",
        'TWB64 262 - Metallic Paldea Brass' => "TWB64 262 - Metallic Paldea Brass",
        'TWB64 263 - Classy Christmas' => "TWB64 263 - Classy Christmas",
        'TWB64 264 - Winter Christmas' => "TWB64 264 - Winter Christmas",
        'TWB64 265 - IDOL WORLD TRICOLOR!!!' => "TWB64 265 - IDOL WORLD TRICOLOR!!!",
        'TWB64 266 - Inkling Tricolor' => "TWB64 266 - Inkling Tricolor",
        'TWB64 267 - 7-Eleven Color Combo' => "TWB64 267 - 7-Eleven Color Combo",
        'TWB64 268 - PAC-PALETTE' => "TWB64 268 - PAC-PALETTE",
        'TWB64 269 - Vulnerable Blue' => "TWB64 269 - Vulnerable Blue",
        'TWB64 270 - Nightvision Green' => "TWB64 270 - Nightvision Green",
        'TWB64 271 - Bandai Namco Tricolor' => "TWB64 271 - Bandai Namco Tricolor",
        'TWB64 272 - Gold, Silver, and Bronze' => "TWB64 272 - Gold, Silver, and Bronze",
        'TWB64 273 - Deku Vigilante Palette' => "TWB64 273 - Deku Vigilante Palette",
        'TWB64 274 - Super Famicom Supreme' => "TWB64 274 - Super Famicom Supreme",
        'TWB64 275 - Absorbent and Yellow' => "TWB64 275 - Absorbent and Yellow",
        'TWB64 276 - 765PRO TRICOLOR' => "TWB64 276 - 765PRO TRICOLOR",
        'TWB64 277 - GameCube Glimmer' => "TWB64 277 - GameCube Glimmer",
        'TWB64 278 - 1st Vision Pastel' => "TWB64 278 - 1st Vision Pastel",
        'TWB64 279 - Perfect Majin Emperor' => "TWB64 279 - Perfect Majin Emperor",
        'TWB64 280 - J-Pop Idol Sherbet' => "TWB64 280 - J-Pop Idol Sherbet",
        'TWB64 281 - Ryuuguu Sunset' => "TWB64 281 - Ryuuguu Sunset",
        'TWB64 282 - Tropical Starfall' => "TWB64 282 - Tropical Starfall",
        'TWB64 283 - Colorful Horizons' => "TWB64 283 - Colorful Horizons",
        'TWB64 284 - BLACKPINK BLINK PINK' => "TWB64 284 - BLACKPINK BLINK PINK",
        'TWB64 285 - DMG-SWITCH' => "TWB64 285 - DMG-SWITCH",
        'TWB64 286 - POCKET SWITCH' => "TWB64 286 - POCKET SWITCH",
        'TWB64 287 - Sunny Passion Paradise' => "TWB64 287 - Sunny Passion Paradise",
        'TWB64 288 - Saiyan Beast Silver' => "TWB64 288 - Saiyan Beast Silver",
        'TWB64 289 - RADIANT SMILE RAMP' => "TWB64 289 - RADIANT SMILE RAMP",
        'TWB64 290 - A-RISE BLUE' => "TWB64 290 - A-RISE BLUE",
        'TWB64 291 - TROPICAL TWICE APRICOT' => "TWB64 291 - TROPICAL TWICE APRICOT",
        'TWB64 292 - Odyssey Boy' => "TWB64 292 - Odyssey Boy",
        'TWB64 293 - Frog Coin Green' => "TWB64 293 - Frog Coin Green",
        'TWB64 294 - Garfield Vision' => "TWB64 294 - Garfield Vision",
        'TWB64 295 - Bedrock Caveman Vision' => "TWB64 295 - Bedrock Caveman Vision",
        'TWB64 296 - BANGTAN ARMY PURPLE' => "TWB64 296 - BANGTAN ARMY PURPLE",
        'TWB64 297 - LE SSERAFIM FEARLESS BLUE' => "TWB64 297 - LE SSERAFIM FEARLESS BLUE",
        'TWB64 298 - Baja Blast Beach' => "TWB64 298 - Baja Blast Beach",
        'TWB64 299 - 3DS Virtual Console Green' => "TWB64 299 - 3DS Virtual Console Green",
        'TWB64 300 - Wonder Purple' => "TWB64 300 - Wonder Purple",
      ],
      '#default_value' => $gambatte_options['gambatte_gb_palette_twb64_3'],
    ];

    $form['gambatte_options']['gambatte_rumble_level'] = [
      '#type' => 'select',
      '#title' => 'gambatte_rumble_level',
      '#options' => [],
      '#default_value' => $gambatte_options['gambatte_rumble_level'],
    ];
    for ($i = 0; $i <= 10; $i++) {
      $form['gambatte_options']['gambatte_rumble_level']['#options'][strval($i)] = strval($i);
    }

    $form['gambatte_options']['gambatte_turbo_period'] = [
      '#type' => 'select',
      '#title' => 'gambatte_turbo_period',
      '#options' => [],
      '#default_value' => $gambatte_options['gambatte_turbo_period'],
    ];
    for ($i = 1; $i <= 120; $i++) {
      $form['gambatte_options']['gambatte_turbo_period']['#options'][strval($i)] = strval($i);
    }

    // Mgba options.
    $mgba_options = $this->getSetting('mgba_options');
    $form['mgba_options'] = [
      '#type' => 'details',
      '#title' => 'mgba Options',
      '#weight' => -3,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][core_options][EJS_core]"]' => ['value' => 'mgba'],
        ],
      ],
    ];

    $form['mgba_options']['mgba_color_correction'] = [
      '#type' => 'select',
      '#title' => 'mgba_color_correction',
      '#options' => [
        'OFF' => "OFF",
        'Auto' => "Auto",
      ],
      '#default_value' => $mgba_options['mgba_color_correction'],
    ];

    $form['mgba_options']['mgba_force_gbp'] = [
      '#type' => 'select',
      '#title' => 'mgba_force_gbp',
      '#options' => [
        'OFF' => "OFF",
        'ON' => "ON",
      ],
      '#default_value' => $mgba_options['mgba_force_gbp'],
    ];

    $form['mgba_options']['mgba_frameskip_interval'] = [
      '#type' => 'select',
      '#title' => 'mgba_frameskip_interval',
      '#options' => [],
      '#default_value' => $mgba_options['mgba_frameskip_interval'],
    ];
    for ($i = 0; $i <= 10; $i++) {
      $form['mgba_options']['mgba_frameskip_interval']['#options'][strval($i)] = strval($i);
    }

    $form['mgba_options']['mgba_frameskip_threshold'] = [
      '#type' => 'select',
      '#title' => 'mgba_frameskip_threshold',
      '#options' => [],
      '#default_value' => $mgba_options['mgba_frameskip_threshold'],
    ];
    for ($i = 15; $i <= 60; $i += 3) {
      $form['mgba_options']['mgba_frameskip_threshold']['#options'][strval($i)] = strval($i);
    }

    $form['mgba_options']['mgba_gb_colors'] = [
      '#type' => 'select',
      '#title' => 'mgba_gb_colors',
      '#options' => [
        'DMG Green' => "DMG Green",
        'GB Light' => "GB Light",
        'GB Pocket' => "GB Pocket",
        'GBC Blue ←' => "GBC Blue ←",
        'GBC Brown ↑' => "GBC Brown ↑",
        'GBC Dark Blue ←A' => "GBC Dark Blue ←A",
        'GBC Dark Brown ↑B' => "GBC Dark Brown ↑B",
        'GBC Dark Green →A' => "GBC Dark Green →A",
        'GBC Gray ←B' => "GBC Gray ←B",
        'GBC Green →' => "GBC Green →",
        'GBC Orange ↓A' => "GBC Orange ↓A",
        'GBC Pale Yellow ↓' => "GBC Pale Yellow ↓",
        'GBC Red ↑A' => "GBC Red ↑A",
        'GBC Reverse →B' => "GBC Reverse →B",
        'GBC Yellow ↓B' => "GBC Yellow ↓B",
        'Grayscale' => "Grayscale",
        'SGB 1-A' => "SGB 1-A",
        'SGB 1-B' => "SGB 1-B",
        'SGB 1-C' => "SGB 1-C",
        'SGB 1-D' => "SGB 1-D",
        'SGB 1-E' => "SGB 1-E",
        'SGB 1-F' => "SGB 1-F",
        'SGB 1-G' => "SGB 1-G",
        'SGB 1-H' => "SGB 1-H",
        'SGB 2-A' => "SGB 2-A",
        'SGB 2-B' => "SGB 2-B",
        'SGB 2-C' => "SGB 2-C",
        'SGB 2-D' => "SGB 2-D",
        'SGB 2-E' => "SGB 2-E",
        'SGB 2-F' => "SGB 2-F",
        'SGB 2-G' => "SGB 2-G",
        'SGB 2-H' => "SGB 2-H",
        'SGB 3-A' => "SGB 3-A",
        'SGB 3-B' => "SGB 3-B",
        'SGB 3-C' => "SGB 3-C",
        'SGB 3-D' => "SGB 3-D",
        'SGB 3-E' => "SGB 3-E",
        'SGB 3-F' => "SGB 3-F",
        'SGB 3-G' => "SGB 3-G",
        'SGB 3-H' => "SGB 3-H",
        'SGB 4-A' => "SGB 4-A",
        'SGB 4-B' => "SGB 4-B",
        'SGB 4-C' => "SGB 4-C",
        'SGB 4-D' => "SGB 4-D",
        'SGB 4-E' => "SGB 4-E",
        'SGB 4-F' => "SGB 4-F",
        'SGB 4-G' => "SGB 4-G",
        'SGB 4-H' => "SGB 4-H",
      ],
      '#default_value' => $mgba_options['mgba_gb_colors'],
    ];

    $form['mgba_options']['mgba_gb_model'] = [
      '#type' => 'select',
      '#title' => 'mgba_gb_model',
      '#options' => [
        'Autodetect' => "Autodetect",
        'Game Boy' => "Game Boy",
        'Super Game Boy' => "Super Game Boy",
        'Game Boy Color' => "Game Boy Color",
        'Game Boy Advance' => "Game Boy Advance",
      ],
      '#default_value' => $mgba_options['mgba_gb_model'],
    ];

    $form['mgba_options']['mgba_idle_optimization'] = [
      '#type' => 'select',
      '#title' => 'mgba_idle_optimization',
      '#options' => [
        'Remove Known' => "Remove Known",
        'Detect and Remove' => "Detect and Remove",
        'Don\'t Remove' => "Don't Remove",
      ],
      '#default_value' => $mgba_options['mgba_idle_optimization'],
    ];

    $form['mgba_options']['mgba_sgb_borders'] = [
      '#type' => 'select',
      '#title' => 'mgba_sgb_borders',
      '#options' => [
        'ON' => "ON",
        'OFF' => "OFF",
      ],
      '#default_value' => $mgba_options['mgba_sgb_borders'],
    ];

    $form['mgba_options']['mgba_skip_bios'] = [
      '#type' => 'select',
      '#title' => 'mgba_skip_bios',
      '#options' => [
        'OFF' => "OFF",
        'ON' => "ON",
      ],
      '#default_value' => $mgba_options['mgba_skip_bios'],
    ];

    $form['mgba_options']['mgba_solar_sensor_level'] = [
      '#type' => 'select',
      '#title' => 'mgba_solar_sensor_level',
      '#options' => [],
      '#default_value' => $mgba_options['mgba_solar_sensor_level'],
    ];
    for ($i = 0; $i <= 10; $i++) {
      $form['mgba_options']['mgba_solar_sensor_level']['#options'][strval($i)] = strval($i);
    }

    $form['mgba_options']['mgba_use_bios'] = [
      '#type' => 'select',
      '#title' => 'mgba_use_bios',
      '#options' => [
        'ON' => "ON",
        'OFF' => "OFF",
      ],
      '#default_value' => $mgba_options['mgba_use_bios'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $path = $file->createFileUrl(TRUE);

      $main_options = $this->getSetting('main_options');
      $game_options = $this->getSetting('game_options');
      $core_options = $this->getSetting('core_options');
      $ui_options = $this->getSetting('ui_options');
      $advanced_options = $this->getSetting('advanced_options');
      $debug_options = $this->getSetting('debug_options');

      $core = $core_options['EJS_core'];
      $uid = str_replace('-', '_', Html::getUniqueId("emulatorjs-gb-$core"));

      // Render each element as markup.
      $elements[$delta] = [
        '#markup' => '<div class="ejs-gb ejs-gb-' . $core . '"><div id="' . $uid . '"></div></div>',
        '#attached' => [
          'library' => ['emulatorjs/emulatorjs', 'ejs_gb/gb'],
          'drupalSettings' => [
            'emulatorjs' => [
              'EJS_pathtodata' => '/' . \Drupal::service('extension.list.module')->getPath('emulatorjs') . '/data/',
              'uid' => $uid,
              'EJS_player' => '#' . $uid,
              'EJS_gameUrl' => $path,
              'EJS_language' => $main_options['EJS_language'],
              'EJS_volume' => $main_options['EJS_volume'],

              'EJS_fullscreenOnLoaded' => $game_options['EJS_fullscreenOnLoaded'] ? 'TRUE' : 'FALSE',
              'EJS_startOnLoaded' => $game_options['EJS_startOnLoaded'] ? 'TRUE' : 'FALSE',

              'EJS_core' => $core_options['EJS_core'],

              'EJS_color' => $ui_options['EJS_color'],
              'EJS_alignStartButton' => $ui_options['EJS_alignStartButton'],
              'EJS_backgroundImage' => $ui_options['EJS_backgroundImage'],
              'EJS_backgroundBlur' => $ui_options['EJS_backgroundBlur']? 'TRUE' : 'FALSE',
              'EJS_backgroundColor' => $ui_options['EJS_backgroundColor'],
              'EJS_AdUrl' => $ui_options['EJS_AdUrl'],
              'EJS_AdTimer' => $ui_options['EJS_AdTimer'],
              'EJS_AdMode' => $ui_options['EJS_AdMode'],

              'EJS_AdSize' => [
                $ui_options['EJS_AdSizeWidth'],
                $ui_options['EJS_AdSizeHeight'],
              ],

              'EJS_CacheLimit' => $advanced_options['EJS_CacheLimit'],
              'EJS_Buttons' => [
                'restart' => $advanced_options['EJS_Button_restart'] ? 'TRUE' : 'FALSE',
                'playPause' => $advanced_options['EJS_Button_playPause'] ? 'TRUE' : 'FALSE',
                'saveState' => $advanced_options['EJS_Button_saveState'] ? 'TRUE' : 'FALSE',
                'loadState' => $advanced_options['EJS_Button_loadState'] ? 'TRUE' : 'FALSE',
                'gamepad' => $advanced_options['EJS_Button_gamepad'] ? 'TRUE' : 'FALSE',
                'cheat' => $advanced_options['EJS_Button_cheat'] ? 'TRUE' : 'FALSE',
                'cacheManager' => $advanced_options['EJS_Button_cacheManager'] ? 'TRUE' : 'FALSE',
                'saveSavFiles' => $advanced_options['EJS_Button_saveSavFiles'] ? 'TRUE' : 'FALSE',
                'loadSavFiles' => $advanced_options['EJS_Button_loadSavFiles'] ? 'TRUE' : 'FALSE',
                'mute' => $advanced_options['EJS_Button_mute'] ? 'TRUE' : 'FALSE',
                'volume' => $advanced_options['EJS_Button_volume'] ? 'TRUE' : 'FALSE',
                'settings' => $advanced_options['EJS_Button_settings'] ? 'TRUE' : 'FALSE',
                'screenshot' => $advanced_options['EJS_Button_screenshot'] ? 'TRUE' : 'FALSE',
                'screenRecord' => $advanced_options['EJS_Button_screenRecord'] ? 'TRUE' : 'FALSE',
                'quickSave' => $advanced_options['EJS_Button_quickSave'] ? 'TRUE' : 'FALSE',
                'quickLoad' => $advanced_options['EJS_Button_quickLoad'] ? 'TRUE' : 'FALSE',
                'fullscreen' => $advanced_options['EJS_Button_fullscreen'] ? 'TRUE' : 'FALSE',
              ],

              'EJS_defaultOptions' => [
                'fastForward' => $core_options['fastForward'],
                'fps' => $core_options['fps'],
                'ff-ratio' => $core_options['ff_ratio'],
                'rewind_granularity' => $core_options['rewind_granularity'],
                'rewindEnabled' => $core_options['rewindEnabled'],
                'save_state_location' => $core_options['save_state_location'],
                'save_state_slot' => $core_options['save_state_slot'],
                'shader' => $core_options['shader'],
                'slowMotion' => $core_options['slowMotion'],
                'sm_ratio' => $core_options['sm_ratio'],
              ],

              'EJS_DEBUG_XX' => $debug_options['EJS_DEBUG_XX'] ? 'TRUE' : 'FALSE',
              'EJS_noAutoFocus' => $debug_options['EJS_noAutoFocus'] ? 'TRUE' : 'FALSE',
              'EJS_settingsLanguage' => $debug_options['EJS_settingsLanguage'] ? 'TRUE' : 'FALSE',
              'EJS_softLoad' => $debug_options['EJS_softLoad'] ? 'TRUE' : 'FALSE',
              'EJS_startButtonName' => $debug_options['EJS_startButtonName'],
            ],
          ],
        ],
      ];

      switch ($core) {
        case 'gambatte':
          $gambatte_options = $this->getSetting('gambatte_options');

          foreach ($gambatte_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

        case 'mgba':
          $mgba_options = $this->getSetting('mgba_options');

          foreach ($mgba_options as $setting => $value) {
            $elements[$delta]['#attached']['drupalSettings']['emulatorjs']['EJS_defaultOptions'][$setting] = $value;
          }

          break;

      }
    }

    return $elements;
  }

}
