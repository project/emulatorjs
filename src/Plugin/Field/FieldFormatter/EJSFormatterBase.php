<?php

namespace Drupal\emulatorjs\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Base class for EJS file formatters.
 */
abstract class EJSFormatterBase extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['main_options'] = [
      'EJS_language' => 'en-US',
      'EJS_volume' => 0.5,
    ];
    $settings['game_options'] = [
      'EJS_fullscreenOnLoaded' => FALSE,
      'EJS_startOnLoaded' => FALSE,
    ];
    $settings['core_options'] = [
      'fastForward' => 'disabled',
      'fps' => 'hide',
      'ff_ratio' => '3.0',
      'rewind_granularity' => '6',
      'rewindEnabled' => 'disabled',
      'save_state_location' => 'download',
      'save_state_slot' => '1',
      'shader' => 'disabled',
      'slowMotion' => 'disabled',
      'sm_ratio' => '3.0',
    ];
    $settings['ui_options'] = [
      'EJS_color' => '#1AAFFF',
      'EJS_alignStartButton' => 'bottom',
      'EJS_backgroundImage' => '',
      'EJS_backgroundBlur' => FALSE,
      'EJS_backgroundColor' => '#333333',
      'EJS_AdUrl' => '',
      'EJS_AdTimer' => '10000',
      'EJS_AdMode' => '2',
      // 'EJS_AdSize' => ['300px', '250px'],
      'EJS_AdSizeWidth' => '300px',
      'EJS_AdSizeHeight' => '250px',
    ];
    $settings['advanced_options'] = [
      'EJS_CacheLimit' => '1073741824',
      'EJS_Button_playPause' => TRUE,
      'EJS_Button_restart' => TRUE,
      'EJS_Button_mute' => TRUE,
      'EJS_Button_settings' => TRUE,
      'EJS_Button_fullscreen' => TRUE,
      'EJS_Button_saveState' => TRUE,
      'EJS_Button_loadState' => TRUE,
      'EJS_Button_screenRecord' => TRUE,
      'EJS_Button_gamepad' => TRUE,
      'EJS_Button_cheat' => TRUE,
      'EJS_Button_volume' => TRUE,
      'EJS_Button_saveSavFiles' => TRUE,
      'EJS_Button_loadSavFiles' => TRUE,
      'EJS_Button_quickSave' => TRUE,
      'EJS_Button_quickLoad' => TRUE,
      'EJS_Button_screenshot' => TRUE,
      'EJS_Button_cacheManager' => TRUE,

    ];
    $settings['debug_options'] = [
      'EJS_DEBUG_XX' => FALSE,
      'EJS_noAutoFocus' => FALSE,
      'EJS_settingsLanguage' => FALSE,
      'EJS_softLoad' => FALSE,
      'EJS_startButtonName' => 'Start Game',
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // Main options.
    $main_options = $this->getSetting('main_options');
    $form['main_options'] = [
      '#type' => 'details',
      '#title' => 'Main Options',
      '#weight' => -6,
    ];

    $form['main_options']['EJS_language'] = [
      '#type' => 'select',
      '#title' => 'EJS_language',
      // '#weight' => 10,
      '#description' => 'Set the emulator UI to a certian language.',
      '#description_display' => 'before',
      '#options' => [
        'en-US' => 'English US',
        'pt-BR' => 'Portuguese',
        'es-ES' => 'Spanish',
        'el-GR' => 'Greek',
        'ja-JA' => 'Japanese',
        'zh-CN' => 'Chinese',
        'hi-HI' => 'Hindi',
        'ar-AR' => 'Arabic',
        'jv-JV' => 'Javanese',
        'ben-BEN' => 'Bengali',
        'ru-RU' => 'Russian',
        'de-GER' => 'German',
        'ko-KO' => 'Korean',
        'af-FR' => 'French',
      ],
      '#default_value' => $main_options['EJS_language'],
    ];

    $form['main_options']['EJS_volume'] = [
      '#type' => 'range',
      '#title' => 'EJS_volume',
      // '#weight' => 10,
      '#description' => 'Sets the default volume for the emulator.',
      '#description_display' => 'before',
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.1,
      '#default_value' => $main_options['EJS_volume'],
    ];

    // Game options.
    $game_options = $this->getSetting('game_options');
    $form['game_options'] = [
      '#type' => 'details',
      '#title' => 'Game Options',
      '#weight' => -5,
    ];

    $form['game_options']['EJS_fullscreenOnLoaded'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_fullscreenOnLoaded',
      // '#weight' => 10,
      '#description' => 'Start the game in fullscreen mode.',
      '#default_value' => $game_options['EJS_fullscreenOnLoaded'],
    ];

    $form['game_options']['EJS_startOnLoaded'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_startOnLoaded',
      // '#weight' => 10,
      '#description' => 'Start the game when the page is loaded. Note that if the user has not interacted with the page, the emulator will freeze until they have done so.',
      '#default_value' => $game_options['EJS_startOnLoaded'],
    ];

    // Core options.
    $core_options = $this->getSetting('core_options');
    $form['core_options'] = [
      '#type' => 'details',
      '#title' => 'Core Options',
      '#weight' => -4,
    ];

    $form['core_options']['EJS_core'] = [
      '#type' => 'select',
      '#title' => 'EJS_core',
      '#weight' => -2,
      '#description' => 'Desired target system.',
      '#description_display' => 'before',
      '#options' => [],
      '#default_value' => $core_options['EJS_core'],
    ];

    $form['core_options']['fastForward'] = [
      '#type' => 'select',
      '#title' => 'fastForward',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        'enabled' => $this->t('Enabled'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $core_options['fastForward'],
    ];

    $form['core_options']['fps'] = [
      '#type' => 'select',
      '#title' => 'fps',
      // '#description' => 'Show frames per second.',
      // '#description_display' => 'before',
      '#options' => [
        'show' => $this->t('show'),
        'hide' => $this->t('hide'),
      ],
      '#default_value' => $core_options['fps'],
    ];

    $form['core_options']['ff_ratio'] = [
      '#type' => 'select',
      '#title' => 'ff-ratio',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '1.5' => '1.5',
        '2.0' => '2.0',
        '2.5' => '2.5',
        '3.0' => '3.0',
        '3.5' => '3.5',
        '4.0' => '4.0',
        '4.5' => '4.5',
        '5.0' => '5.0',
        '5.5' => '5.5',
        '6.0' => '6.0',
        '6.5' => '6.5',
        '7.0' => '7.0',
        '7.5' => '7.5',
        '8.0' => '8.0',
        '8.5' => '8.5',
        '9.0' => '9.0',
        '9.5' => '9.5',
        '10.0' => '10.0',
        'unlimited' => $this->t('unlimited'),
      ],
      '#default_value' => $core_options['ff_ratio'],
    ];

    $form['core_options']['rewind_granularity'] = [
      '#type' => 'select',
      '#title' => 'rewind-granularity',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '1' => '1',
        '3' => '3',
        '6' => '6',
        '12' => '12',
        '25' => '25',
        '50' => '50',
        '100' => '100',
      ],
      '#default_value' => $core_options['rewind_granularity'],
    ];

    $form['core_options']['rewindEnabled'] = [
      '#type' => 'select',
      '#title' => 'rewindEnabled',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        'enabled' => $this->t('Enabled'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $core_options['rewindEnabled'],
    ];

    $form['core_options']['save_state_location'] = [
      '#type' => 'select',
      '#title' => 'save-state-location',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        'browser' => $this->t('Keep in Browser'),
        'download' => $this->t('Download'),
      ],
      '#default_value' => $core_options['save_state_location'],
    ];

    $form['core_options']['save_state_slot'] = [
      '#type' => 'select',
      '#title' => 'save-state-slot',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [],
      '#default_value' => $core_options['save_state_slot'],
    ];
    for ($i = 1; $i <= 9; $i++) {
      $form['core_options']['save_state_slot']['#options'][strval($i)] = strval($i);
    }

    $form['core_options']['shader'] = [
      '#type' => 'select',
      '#title' => 'shader',
      // '#description' => 'Shader.',
      // '#description_display' => 'before',
      '#options' => [
        '2xScaleHQ.glslp' => "2xScaleHQ",
        '4xScaleHQ.glslp' => "4xScaleHQ",
        'crt-aperture.glslp' => "CRT aperture",
        'crt-easymode.glslp' => "CRT easymode",
        'crt-geom.glslp' => "CRT geom",
        'crt-mattias.glslp' => "CRT mattias",
        'disabled' => "Disabled",
      ],
      '#default_value' => $core_options['shader'],
    ];

    $form['core_options']['slowMotion'] = [
      '#type' => 'select',
      '#title' => 'slowMotion',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        'enabled' => $this->t('Enabled'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $core_options['slowMotion'],
    ];

    $form['core_options']['sm_ratio'] = [
      '#type' => 'select',
      '#title' => 'sm_ratio',
      // '#description' => '',
      // '#description_display' => 'before',
      '#options' => [
        '1.5' => '1.5',
        '2.0' => '2.0',
        '2.5' => '2.5',
        '3.0' => '3.0',
        '3.5' => '3.5',
        '4.0' => '4.0',
        '4.5' => '4.5',
        '5.0' => '5.0',
        '5.5' => '5.5',
        '6.0' => '6.0',
        '6.5' => '6.5',
        '7.0' => '7.0',
        '7.5' => '7.5',
        '8.0' => '8.0',
        '8.5' => '8.5',
        '9.0' => '9.0',
        '9.5' => '9.5',
        '10.0' => '10.0',
      ],
      '#default_value' => $core_options['sm_ratio'],
    ];

    // UI options.
    $ui_options = $this->getSetting('ui_options');
    $form['ui_options'] = [
      '#type' => 'details',
      '#title' => 'UI Options',
      '#weight' => -2,
    ];

    $form['ui_options']['EJS_color'] = [
      '#type' => 'color',
      '#title' => 'EJS_color',
      '#description' => 'Emulator hex color theme.',
      '#description_display' => 'before',
      '#default_value' => $ui_options['EJS_color'],
    ];

    $form['ui_options']['EJS_alignStartButton'] = [
      '#type' => 'select',
      '#title' => 'EJS_alignStartButton',
      '#description' => 'This can be used to align the start button.',
      '#description_display' => 'before',
      '#options' => [
        'top' => 'top',
        'center' => 'center',
        'bottom' => 'bottom',
      ],
      '#default_value' => $ui_options['EJS_alignStartButton'],
    ];

    $form['ui_options']['EJS_backgroundImage'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_backgroundImage',
      '#description' => 'Url to a file you want to have as the background at the "Play Now" screen. Must be a Absolute path or a Relative Path from the pathtodata folder.',
      '#description_display' => 'before',
      '#size' => 30,
      '#default_value' => $ui_options['EJS_backgroundImage'],
    ];

    $form['ui_options']['EJS_backgroundBlur'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_backgroundBlur',
      '#description' => 'This will blur the background image to fit the all aspect ratios. Also known as frame blur. This will override the EJS_backgroundColor.',
      '#default_value' => $ui_options['EJS_backgroundBlur'],
    ];

    $form['ui_options']['EJS_backgroundColor'] = [
      '#type' => 'color',
      '#title' => 'EJS_backgroundColor',
      '#description' => 'This will set the background color of the emulator. In the start and loadings screens.',
      '#description_display' => 'before',
      '#default_value' => $ui_options['EJS_backgroundColor'],
    ];

    $form['ui_options']['EJS_AdUrl'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_AdUrl',
      '#description' => 'URL to ad page. Will show this page when the page is loaded.',
      '#description_display' => 'before',
      '#size' => 30,
      '#default_value' => $ui_options['EJS_AdUrl'],
    ];

    $form['ui_options']['EJS_AdTimer'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_AdTimer',
      '#description' => 'Duration (in milliseconds) the ad iframe will stay before it automatically closes. Setting to 0 will disable auto-close. Setting to -1 will close the ad immediately.',
      '#description_display' => 'before',
      '#size' => 10,
      '#default_value' => $ui_options['EJS_AdTimer'],
    ];

    $form['ui_options']['EJS_AdMode'] = [
      '#type' => 'select',
      '#title' => 'EJS_AdMode',
      '#description' => 'This will set the ad mode.',
      '#description_display' => 'before',
      '#options' => [
        '0' => 'show the ad on the start screen',
        '1' => 'show the ad on the loading screen',
        '2' => 'show on both the start and loading screen',
      ],
      '#default_value' => $ui_options['EJS_AdMode'],
    ];

    $form['ui_options']['EJS_AdSizeWidth'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_AdSizeWidth',
      '#description' => 'This will set the size of the ad. It can be any css option %, vh, px etc.',
      '#description_display' => 'before',
      '#size' => 10,
      '#default_value' => $ui_options['EJS_AdSizeWidth'],
    ];

    $form['ui_options']['EJS_AdSizeHeight'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_AdSizeHeight',
      // '#description' => '',
      // '#description_display' => 'before',
      '#size' => 10,
      '#default_value' => $ui_options['EJS_AdSizeHeight'],
    ];

    // Advanced options.
    $advanced_options = $this->getSetting('advanced_options');
    $form['advanced_options'] = [
      '#type' => 'details',
      '#title' => 'Advanced Options',
      '#weight' => -1,
    ];

    $form['advanced_options']['EJS_CacheLimit'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_CacheLimit',
      '#description' => 'The limit to game cache (per rom) in bytes.',
      '#description_display' => 'before',
      '#size' => 10,
      '#default_value' => $advanced_options['EJS_CacheLimit'],
    ];

    $form['advanced_options']['button_heading'] = [
      '#prefix' => '<label>',
      '#suffix' => '</label>',
      '#markup' => 'EJSButtons',
    ];

    $form['advanced_options']['EJS_Button_restart'] = [
      '#type' => 'checkbox',
      '#title' => 'restart',
      '#default_value' => $advanced_options['EJS_Button_restart'],
    ];

    $form['advanced_options']['EJS_Button_playPause'] = [
      '#type' => 'checkbox',
      '#title' => 'playPause',
      // '#description' => '',
      '#default_value' => $advanced_options['EJS_Button_playPause'],
    ];

    $form['advanced_options']['EJS_Button_saveState'] = [
      '#type' => 'checkbox',
      '#title' => 'saveState',
      '#default_value' => $advanced_options['EJS_Button_saveState'],
    ];

    $form['advanced_options']['EJS_Button_loadState'] = [
      '#type' => 'checkbox',
      '#title' => 'loadState',
      '#default_value' => $advanced_options['EJS_Button_loadState'],
    ];

    $form['advanced_options']['EJS_Button_gamepad'] = [
      '#type' => 'checkbox',
      '#title' => 'gamepad',
      '#default_value' => $advanced_options['EJS_Button_gamepad'],
    ];

    $form['advanced_options']['EJS_Button_cheat'] = [
      '#type' => 'checkbox',
      '#title' => 'cheat',
      '#default_value' => $advanced_options['EJS_Button_cheat'],
    ];

    $form['advanced_options']['EJS_Button_cacheManager'] = [
      '#type' => 'checkbox',
      '#title' => 'cacheManager',
      '#default_value' => $advanced_options['EJS_Button_cacheManager'],
    ];

    $form['advanced_options']['EJS_Button_saveSavFiles'] = [
      '#type' => 'checkbox',
      '#title' => 'saveSavFiles',
      '#default_value' => $advanced_options['EJS_Button_saveSavFiles'],
    ];

    $form['advanced_options']['EJS_Button_loadSavFiles'] = [
      '#type' => 'checkbox',
      '#title' => 'loadSavFiles',
      '#default_value' => $advanced_options['EJS_Button_loadSavFiles'],
    ];

    $form['advanced_options']['EJS_Button_mute'] = [
      '#type' => 'checkbox',
      '#title' => 'mute',
      '#default_value' => $advanced_options['EJS_Button_mute'],
    ];

    $form['advanced_options']['EJS_Button_volume'] = [
      '#type' => 'checkbox',
      '#title' => 'volume',
      '#default_value' => $advanced_options['EJS_Button_volume'],
    ];

    $form['advanced_options']['EJS_Button_settings'] = [
      '#type' => 'checkbox',
      '#title' => 'settings',
      '#default_value' => $advanced_options['EJS_Button_settings'],
    ];

    $form['advanced_options']['EJS_Button_screenshot'] = [
      '#type' => 'checkbox',
      '#title' => 'screenshot',
      '#default_value' => $advanced_options['EJS_Button_screenshot'],
    ];

    $form['advanced_options']['EJS_Button_screenRecord'] = [
      '#type' => 'checkbox',
      '#title' => 'screenRecord',
      '#default_value' => $advanced_options['EJS_Button_screenRecord'],
    ];

    $form['advanced_options']['EJS_Button_quickSave'] = [
      '#type' => 'checkbox',
      '#title' => 'quickSave',
      '#default_value' => $advanced_options['EJS_Button_quickSave'],
    ];

    $form['advanced_options']['EJS_Button_quickLoad'] = [
      '#type' => 'checkbox',
      '#title' => 'quickLoad',
      '#default_value' => $advanced_options['EJS_Button_quickLoad'],
    ];

    $form['advanced_options']['EJS_Button_fullscreen'] = [
      '#type' => 'checkbox',
      '#title' => 'fullscreen',
      '#default_value' => $advanced_options['EJS_Button_fullscreen'],
    ];

    // Debug options.
    $debug_options = $this->getSetting('debug_options');
    $form['debug_options'] = [
      '#type' => 'details',
      '#title' => 'Debug Options',
      '#weight' => 1,
    ];

    $form['debug_options']['EJS_DEBUG_XX'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_DEBUG_XX',
      '#description' => 'Enable debug mode. This will log a lot of information to the console and use the unminified scripts. This is useful for debugging issues with the emulator.',
      '#default_value' => $debug_options['EJS_DEBUG_XX'],
    ];

    $form['debug_options']['EJS_noAutoFocus'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_noAutoFocus',
      '#description' => 'Prevents automatically focusing the frame.',
      '#default_value' => $debug_options['EJS_noAutoFocus'],
    ];

    $form['debug_options']['EJS_settingsLanguage'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_settingsLanguage',
      '#description' => 'For debugging and adding new languages. Set it to true and it will enable the missing translations in the console logs.',
      '#default_value' => $debug_options['EJS_settingsLanguage'],
    ];

    $form['debug_options']['EJS_softLoad'] = [
      '#type' => 'checkbox',
      '#title' => 'EJS_softLoad',
      '#description' => 'Automatically reset the console after x seconds.',
      '#default_value' => $debug_options['EJS_softLoad'],
    ];

    $form['debug_options']['EJS_startButtonName'] = [
      '#type' => 'textfield',
      '#title' => 'EJS_startButtonName',
      '#description' => 'Custom text for the start button.',
      '#description_display' => 'before',
      '#size' => 30,
      '#default_value' => $debug_options['EJS_startButtonName'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $core_options = $this->getSetting('core_options');

    $summary[] = 'EJS_core: ' . $core_options['EJS_core'];

    return $summary;
  }

}
