# EmulatorJS

An [EmulatorJS](https://emulatorjs.org/) file formatter.

> **WARNING:**
> this is pre-alpha and subject to major changes which could result in settings relating to this module being lost. if the module is uninstalled, file fields that were using the formatter will revert to being displayed as generic files.

## Known Issues
- only 1 instance per page due to window.* config variables. this does not work in a view of more than one
  - need to explore iframe option
  - views provides a single formatter per display, unless showing nodes or teasers
- no bios support; many systems currently unusable
- no cheats or patches support
- no input settings (config controls)
- some aspect ratio css are wrong, want a config
  - can remove system level css when config
- many untested/unfinished settings/systems
  - might be some numbers that should be strings (trailing zeros expected)
- volume, shader, etc. settings aren't being used if changed thru ui (localStorage)
- mame2023 brightness and gamma values aren't being used (localStorage?)
  - rom-specific options/dip switches not available

## Installation
composer require the module

download a [release](https://github.com/EmulatorJS/EmulatorJS/releases) of EmulationJS (not the source)
- unzip it somewhere
- [minify](https://emulatorjs.org/docs4devs/Minifying.html) the data (requires NodeJS)
  1. Open a terminal and enter the /data/minify directory
  2. Install the dependencies with:
     `npm install`
  3. Start the minification with:
     `npm run build`
- copy data dir to the module dir, so that loader.js can be found at modules/contrib/**emulatorjs/data/loader.js** (or wherever you have the module)
- set the permissions on the data dir & subdirs (775), and files (664)

enable a system
- for example **Atari 2600**

create a new content type or modify an existing one
- name it something like **Atari 2600**

add a "file" field (this field will be resuable on other systems/content types)
- name it something like **field_rom**, label it **ROM file**
- allowed number of values: **Limited/1**
- set the file extensions to **zip, 7z**
- set the file directory, something like **atari2600/roms**
  - **note:** this will result in Drupal renaming duplicate filenames, so using a token in this path is suggested for systems that care about a zip filename (arcade); something like **atari2600/roms/[current-date:raw]**, **atari2600/roms/[random:number]**, or **atari2600/roms/[random:hash:md5]**. this of course requires Drupal's "Token" module
    - this will however unfortunately result in empty folders if you delete a file/node; need to circle back on this
  - this also relates to the `$config['file.settings']['make_unused_managed_files_temporary']`
- click save and manage fields (requires core "Field UI" module)

click on the "manage display" tab of this content type
- change the format of this new file field to **EmulatorJS Atari 2600**
- click the gear icon to the right of the row for settings
- review/adjust settings
- click update
- click save

create a new node of this content type, upload/attach a rom file, save and view it

## Systems

no bios support yet; systems that require bios can't currently run

*italics*: not implemented yet

|        | Status | BIOS  |
| :---   | :---:  | :---: |
| 3DO    |  | req |
| Arcade | some | ≈ |
| Atari 2600 | ok |  |
| Atari 5200 |  | opt |
| Atari 7800 |  | opt |
| Atari Jaguar |  | opt |
| Atari Lynx |  | req |
| Bandai WonderSwan/Color |  |  |
| ColecoVision |  | req |
| Commodore 64 |  | opt |
| Commodore 128 |  | opt |
| Commodore Amiga |  | opt |
| Commodore PET |  | opt |
| Commodore Plus/4 |  | opt |
| Commodore VIC-20 |  | opt |
| NES/Famicom |  | opt |
| Nintendo 64 |  |  |
| Nintendo DS |  | req |
| Nintendo Game Boy/Advance |  | opt |
| Nintendo Virtual Boy |  |  |
| PC Engine |  |  |
| PC-FX |  |  |
| PlayStation |  | req |
| *Sega 32X* | [broken](https://github.com/EmulatorJS/EmulatorJS/issues/579) |  |
| Sega CD |  | req |
| Sega Game Gear |  | opt |
| Sega Master System |  |  |
| Sega Mega Drive |  |  |
| Sega Saturn |  | opt |
| SNES/Super Famicom |  | opt |
| SNK Neo Geo Pocket/Color |  |  |

## version notes

`EJS_noAutoFocus` is > 4.0.11