((Drupal, settings) => {

  Drupal.behaviors.emulatorjs = {

    attach(context, settings) {

      Object.keys(settings.emulatorjs).forEach((key, index) => {

        if (settings.emulatorjs[key] === 'TRUE') {

          window[key] = true;

        } else if (settings.emulatorjs[key] === 'FALSE') {

          window[key] = false;

        } else if (key === 'EJS_Buttons') {

          window.EJS_Buttons = {};

          Object.keys(settings.emulatorjs.EJS_Buttons).forEach((bkey, bindex) => {

            window.EJS_Buttons[bkey] = settings.emulatorjs.EJS_Buttons[bkey] === 'TRUE' ? true : false;

          });

        } else {

          window[key] = settings.emulatorjs[key];

        }
    
      });

    }

  };

})(Drupal, drupalSettings);
